<?php 

/**
 * 
 * PayPal Express checkout integration service
 * sources-
 * http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/CreatePaymentUsingPayPal.html
 * http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/ExecutePayment.html
 * https://medium.com/justlaravel/how-to-integrate-paypal-payment-gateway-in-laravel-695063599449
 * 
*/

namespace Classiebit\Eventmie\Services;

/**
 * PayPal sdk bootstrap.php
*/
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

/**
 * PayPal sdk classes Part-1 
*/
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

/**
 * PayPal sdk classes Part-2 
 * 
*/
use PayPal\Api\PaymentExecution;

/**
 * For fetching PayPal $_GET callback requests 
*/
use Illuminate\Http\Request;
use Session;
use URL;

class PaypalExpress 
{
    private $_api_context;
    private $_callback_url;

    /**
     * PaypalExpress constructor.
     *
     */
    public function __construct($settings = [])
    {
        $config = [ 
            'client_id' => $settings['paypal_client_id'],
            'secret' => $settings['paypal_secret'],
            'callback_url' => 'bookings/paypal/callback',
            'settings' => [
                'mode' => $settings['paypal_mode'] ? 'live' : 'sandbox',
                'http.ConnectionTimeOut' => 30,
                'log.LogEnabled' => true,
                'log.FileName' => storage_path() . '/logs/paypal.log',
                'log.LogLevel' => 'ERROR'
            ]
        ];

        /** PayPal api context **/
        // init Paypal by verifying through Paypal API credentials
        $this->_api_context = new ApiContext(
            new OAuthTokenCredential(
                $config['client_id'],
                $config['secret']
            )
        );

        
        // after verify, set default settings
        $this->_api_context->setConfig($config['settings']);

        // set callback url
        $this->_callback_url = $config['callback_url'];
    }

    // 1. Validate and create new order request for single item only
    public function create_order($order = [], $currency = 'USD')
    {
        // required params
        if( empty($order['product_title']) ||
            empty($order['item_sku']) ||
            empty($order['price']) ||
            empty($order['price_title']) ||
            empty($order['price_tagline']) ||
            empty($order['order_number'])
        )
            return false;

        // init paypal payment
        $payer      = new Payer();
        $payer->setPaymentMethod('paypal');

        // create an item
        $item       = new Item();
        $item->setName($order['product_title']) /** item name **/
        ->setCurrency($currency)
        ->setQuantity(1)
        ->setSku($order['item_sku'])
        ->setPrice($order['price']); /** unit price **/
        
        // set item into itemlist
        $item_list = new ItemList();
        $item_list->setItems(array($item));

        /* Tax details (skip for now) */
        
        // set total price        
        $amount = new Amount();
        $amount->setCurrency($currency)
        ->setTotal($order['price']);
        
        // create a transaction instance and set description and invoice number
        $transaction = new Transaction();
        $transaction->setAmount($amount)
        ->setItemList($item_list)
        ->setDescription($order['price_title'].'- '.$order['price_tagline'])
        ->setInvoiceNumber($order['order_number']);
        
        // set callback urls
        $redirect_urls = new RedirectUrls();
        $redirect_urls
        ->setReturnUrl(eventmie_url($this->_callback_url)) /** Specify return URL **/
        ->setCancelUrl(eventmie_url($this->_callback_url));
        
        // finally create a payment for paypal checkout url generation
        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirect_urls)
        ->setTransactions(array($transaction));

        try 
        {
            // execute payment 
            $payment->create($this->_api_context);
        } 
        catch (\PayPal\Exception\PPConnectionException $ex) 
        {
            return ['error' => 'Connection timeout, try again!', 'status' => false];
        }
        
        $redirect_url = null;
        foreach ($payment->getLinks() as $link) 
        {
            if ($link->getRel() == 'approval_url') 
            {
                $redirect_url = $link->getHref();
                break;
            }
        }
        
        //  add payment ID as transaction_id into session 
        session(['paypal_payment_id' => $payment->getId()]);
        
        if (!empty($redirect_url)) 
        {
            // url: redirect to paypal for checkout  
            return ['url' => $redirect_url, 'status' => true];
        }
        
        return ['error' => 'Unexpected, connection timeout, try again!', 'status' => false];
    }

    // 2. On return from gateway check if payment fail or success
    public function callback(Request $request)
    {
        // get the previously stored payment_id from session and 
        $payment_id = session('paypal_payment_id');

        // if in return not get PayerID and token, then payment cancelled
        if (!$request->has('PayerID') || !$request->has('token') || empty(session('paypal_payment_id')) ) 
        {
            return [
                'error'         => 'Payment cancelled!', 
                // only for reference
                'transaction_id'=> $payment_id,
                'status'        => false
            ];
        }

        
        // response data from Paypal
        $payer_id   = $request->input('PayerID');
        $token      = $request->input('token');
            
        // final execution after successful payment
        $payment    = Payment::get($payment_id, $this->_api_context);
        $execution  = new PaymentExecution();
        $execution->setPayerId($payer_id);
        
        $result     = $payment->execute($execution, $this->_api_context);

        // final check if return approved: payment success
        if ($result->getState() == 'approved') 
        {
            // set success data
            $success = [
                'transaction_id'    => $payment_id,
                'payer_reference'   => $payer_id,
                'message'           => 'approved',
                'status'            => true,
            ];
            
            return $success;
        }

        // else payment failed
        return [
            // only for reference
            'paymentId'=>$payment_id,
            'error' => 'Payment failed!', 
            'status' => false
        ];
    }    

    
    
}
