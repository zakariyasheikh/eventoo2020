<?php

namespace Classiebit\Eventmie\Commands;

use Illuminate\Console\Command;

use Classiebit\Eventmie\EventmieServiceProvider;


class EseedCommand extends Command{
    /**
       * The name and signature of the console command.
       *
       * @var string
    */
    
    protected $signature = 'eventmie:eseed';
    
    /**
       * The console command description.
       *
       * @var string
    */
    
    protected $description = 'eventmie seeder';
    
    
    /**
       * Execute the console command.
       *
       * @return mixed
    */
    
    public function handle() {
        $this->info('Generating Eventmie Seeders...');

        \Iseed::generateSeed('data_rows');
        $this->info('data_rows seeder generated');

        \Iseed::generateSeed('data_types');
        $this->info('data_types seeder generated');

        \Iseed::generateSeed('menus');
        $this->info('menus seeder generated');

        \Iseed::generateSeed('menu_items');
        $this->info('menu_items seeder generated');

        \Iseed::generateSeed('permissions');
        $this->info('permissions seeder generated');

        \Iseed::generateSeed('permission_role');
        $this->info('permission_role seeder generated');

        $this->info('Eventmie Seeders Generated Successfully! Run -- php artisan db:seed -- to update database.');
    }
 }
