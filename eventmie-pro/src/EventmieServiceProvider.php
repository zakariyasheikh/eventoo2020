<?php

namespace Classiebit\Eventmie;

use Illuminate\Foundation\AliasLoader;
use Classiebit\Eventmie\Facades\Eventmie as EventmieFacade;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

// Voyager serviceProvider
use TCG\Voyager\VoyagerServiceProvider as VoyagerServiceProvider;
use TCG\Voyager\Facades\Voyager;
// Voyager custom form field
use Classiebit\Eventmie\FormFields\OrganiserDropdown;

// Laravel Socialite
use Laravel\Socialite\SocialiteServiceProvider;

// Ziggy service provider
use Tightenco\Ziggy\ZiggyServiceProvider as ZiggyServiceProvider;
use View;
use Config;

// iSeed service provider
use Orangehill\Iseed\IseedServiceProvider as IseedServiceProvider;

// simple-qrcode service provider
use SimpleSoftwareIO\QrCode\QrCodeServiceProvider as QrCodeServiceProvider;

// DomPDFServiceProvider service provider
use Barryvdh\DomPDF\ServiceProvider as DomPDFServiceProvider;

use  Classiebit\Eventmie\Commands\InstallCommand;
use  Classiebit\Eventmie\Commands\ControllersCommand;
use  Classiebit\Eventmie\Commands\EseedCommand;


class EventmieServiceProvider extends ServiceProvider
{
    protected $is_facebook = false;
    protected $is_google   = false;

    
    /**
     * Register the application services.
     */
    public function register()
    {
        // register external packages
        $this->registerPackages();

        // register Eventmie facade
        $loader = AliasLoader::getInstance();
        $loader->alias('Eventmie', EventmieFacade::class);
        $this->app->singleton('eventmie', function () {
            return new Eventmie();
        });

        // boot up all helpers
        $this->loadHelpers();
        
        // boot up config file
        $this->registerConfigs();

        // customise voyager theme
        $this->customVoyagerTheme();

        // custom voyager formfields
        $this->voyagerFormFields();
        
        // initialise console commands 
        if ($this->app->runningInConsole()) 
        {
            $this->registerPublishableResources();
            $this->registerConsoleCommands();
        }
    }

    /**
     * Bootstrap the application services.
     *
     * @param \Illuminate\Routing\Router $router
     */
    public function boot(Router $router)
    {
        // load middleware
        $this->loadMiddlewares();

        //exception handler
        \App::singleton(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            \Classiebit\Eventmie\Exceptions\MyHandler::class
        );
        
        if (\Schema::hasTable('settings')) 
        {
            // setup mail configs
            $this->mailConfiguration(setting('mail'));
            
            // setup regional settings
            $this->setRegional(setting('regional'));
            
            // Setup oauth 
            $this->socialite(setting('apps'));

        }
        
        // load eventmie resources.views
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'eventmie');

        // load eventmie language files publishable.lang
        $this->loadTranslationsFrom(realpath(__DIR__.'/../publishable/lang'), 'eventmie-pro');
        
        // load eventmie database migrations
        
        if (config('eventmie.database.autoload_migrations', true)) 
            $this->loadMigrationsFrom(realpath(__DIR__.'/../publishable/database/migrations'));
        
    }

    /**
     * Register external package serviceproviders
    */
    private function registerPackages()
    {
        // voyager serviceProvider
        $this->app->register(VoyagerServiceProvider::class);
        
        // socialite serviceProvider
        $this->app->register(SocialiteServiceProvider::class);
        
        // ziggy serviceProvider
        $this->app->register(ZiggyServiceProvider::class);

        // iseed serviceProvider
        $this->app->register(IseedServiceProvider::class);

        // simple-qrcode service provider
        $this->app->register(QrCodeServiceProvider::class);
        
        // DomPDFServiceProvider service provider
        $this->app->register(DomPDFServiceProvider::class);
    }

    /**
     * Load middlewares for user group specific access
     */
    private function loadMiddlewares()
    {
        $this->app['router']->aliasMiddleware('auth', \Classiebit\Eventmie\Middleware\Authenticate::class);
        $this->app['router']->aliasMiddleware('organiser', \Classiebit\Eventmie\Middleware\OrganiserMiddleware::class);
        $this->app['router']->aliasMiddleware('customer', \Classiebit\Eventmie\Middleware\CustomerMiddleware::class);
        $this->app['router']->aliasMiddleware('admin', \Classiebit\Eventmie\Middleware\AdminMiddleware::class);
        $this->app['router']->aliasMiddleware('guest', \Classiebit\Eventmie\Middleware\RedirectIfAuthenticated::class);
        $this->app['router']->aliasMiddleware('common', \Classiebit\Eventmie\Middleware\CommonMiddleware::class);
    }

    /**
     * Load helpers.
     */
    private function loadHelpers()
    {
        foreach (glob(__DIR__.'/Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }

    /**
     * Register the publishable files.
     */
    private function registerPublishableResources()
    {
        $publishablePath    = dirname(__DIR__).'/publishable';

        $publishable        = [
            'config' => [
                "{$publishablePath}/config/eventmie.php" => config_path('eventmie.php')
            ],
            'eventmie-config' => [
                "{$publishablePath}/config/eventmie.php" => config_path('eventmie.php')
            ],
            'resources' => [
                "{$publishablePath}/lang" => resource_path('lang/vendor/eventmie-pro')
            ],
            'storage' => [
                "{$publishablePath}/dummy_content/" => storage_path('app/public')
            ],
        ];
        
        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, $group);
        }

    }

    /**
     * Setup Eventmie configs
      */
    private function registerConfigs()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__).'/publishable/config/eventmie.php', 'eventmie'
        );

        /* ===== OVERRIDE VOYAGER CONFIG WITHOUT PUBLISHING voyager.php TO LARAVEL APP ===== */
        // merge new config with voyager original config
        $voyager_config = dirname(__DIR__).'/publishable/config/voyager.php';
        $this->app['config']->set('voyager', require $voyager_config);
        /* ================================================================================= */
        
    }

    /**
     * Customise voyager theme
      */
    private function customVoyagerTheme()
    {
        $theme_url = eventmie_url().'/eventmie-assets?path='.urlencode('css/voyager.css');
        $this->app['config']->set('voyager.additional_css.custom_theme', $theme_url);
    }

    /**
     * Voyager custom formFields
      */
    public function voyagerFormFields() 
    {
        Voyager::addFormField(OrganiserDropdown::class);
    }

    /**
     * Register the commands accessible from the Console.
     */
    private function registerConsoleCommands()
    {
        $this->commands(Commands\InstallCommand::class);
        $this->commands(Commands\ControllersCommand::class);
        $this->commands(Commands\EseedCommand::class);
        
    }
    
    /**
     * Set socialite configs from admin panel settings
     */
    private function socialite($apis = [])
    {   
        // facebook auth already exists
        if(!empty($this->app['config']['services']['facebook'])) 
        {
            // activate facebook login
            $this->is_facebook = true;
        }
        else
        {
            // facebook auth not exists in app
            // check if facebook app credentials exists in settings

            // setting('apps.facebook_app_id')
            // setting('apps.facebook_app_secret')
            if(!empty($apis['facebook_app_id']) && !empty($apis['facebook_app_secret']))
            {
                $this->app['config']->set('services.facebook', [
                    'client_id' =>  $apis['facebook_app_id'],
                    'client_secret' => $apis['facebook_app_secret'],
                    'redirect' => eventmie_url('login/facebook/callback'),
                    
                ]);

                // activate facebook login
                $this->is_facebook = true;
            }
        }

        // google auth already exists
        if(!empty($this->app['config']['services']['google'])) 
        {
            // activate google login
            $this->is_google = true;
        }
        else
        {
            // google auth not exists in app
            // check if google app credentials exists in settings

            // setting('apps.google_app_id')
            // setting('apps.google_app_secret')
            if(!empty($apis['google_client_id']) && !empty($apis['google_client_secret']))
            {
                $this->app['config']->set('services.google', [
                    'client_id' =>  $apis['google_client_id'],
                    'client_secret' => $apis['google_client_secret'],
                    'redirect' => eventmie_url('login/google/callback'),
                    
                    
                ]);
                
                // activate google login
                $this->is_google = true;
            }
        }

        $data  = [
            'is_google'     => $this->is_google,
            'is_facebook'   => $this->is_facebook,
        ];
        
        View::share('apis', $data);
    }
   
    /**
     *  Set mail configs from admin panel settings
     */
    private function mailConfiguration($mail = [])
    {
        Config::set('mail.host', $mail['mail_host']);
        Config::set('mail.port', $mail['mail_port']);
        Config::set('mail.driver', $mail['mail_driver']);
        Config::set('mail.from', ['address' => $mail['mail_sender_email'],
                                    'name' =>  $mail['mail_sender_name']
                                ]);
        Config::set('mail.encryption', $mail['mail_encryption']);
        Config::set('mail.username', $mail['mail_username']);
        Config::set('mail.password', $mail['mail_password']);
            
    }
     
    /**
     * Regional settings
     * Timezone & Language
     * 
     * Set regional settings from admin panel settings
    */
    private function setRegional($regional = [])
    {
        // set server side timezone
        Config::set('app.timezone', $regional['timezone_default']);

        // change only frontend language
        $default_lang = config('eventmie.default_lang');

        // do not change language on admin panel
        $current_url = url()->current();
        $admin_prefix = config('eventmie.route.admin_prefix');
        if (! (strpos($current_url, $admin_prefix) !== false) )
            $this->app->setLocale($default_lang);
    }

}
