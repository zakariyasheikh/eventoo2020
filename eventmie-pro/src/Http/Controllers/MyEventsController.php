<?php

namespace Classiebit\Eventmie\Http\Controllers;
use App\Http\Controllers\Controller; 
use Facades\Classiebit\Eventmie\Eventmie;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Carbon;
use Carbon\CarbonPeriod;
use Auth;
use Redirect;
use File;
use Classiebit\Eventmie\Models\Booking;
use Classiebit\Eventmie\Models\Event;
use Classiebit\Eventmie\Models\Ticket;
use Classiebit\Eventmie\Models\User;
use Classiebit\Eventmie\Models\Category;
use Classiebit\Eventmie\Models\Country;
use Classiebit\Eventmie\Models\Schedule;
use Classiebit\Eventmie\Models\Speaker;
use Classiebit\Eventmie\Models\Sponsor;
use Classiebit\Eventmie\Models\Tax;
use Classiebit\Eventmie\Notifications\MailNotification;


class MyEventsController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // language change
        $this->middleware('common');

        // exclude routes
        $this->middleware('organiser')->except(['delete_event']);
        
        $this->event    = new Event;

        $this->ticket   = new Ticket;

        $this->category = new Category;

        $this->country  = new Country;

        $this->schedule = new Schedule;

        $this->speaker  = new Speaker;

        $this->sponsor  = new Sponsor;
        
        $this->tax      = new Tax;

        $this->booking  = new Booking;
        
        $this->organiser_id = null;   
    }

    /**
     *  my event for particular ogrganiser
     */

    // only organiser can see own events and admin or customer can't see organiser events 

    public function index()
    {
        // get prifex from eventmie config
        $path = false;
        if(!empty(config('eventmie.route.prefix')))
            $path = config('eventmie.route.prefix');

        // admin can't see organiser bookings
        if(Auth::user()->hasRole('admin'))
        {
            return redirect()->route('voyager.events.index');   
        }

        return Eventmie::view('eventmie::myevents.index', compact('path'));
    }

    /**
     *  my event for particular organiser
     */

    public function get_myevents()
    {
        if(Auth::user()->hasRole('admin'))
        {
            return redirect()->route('voyager.events.index');   
        }

        $params   = [
            'organiser_id' => Auth::id(),
        ];

        $myevents    = $this->event->get_my_events($params);

        if(empty($myevents))
            return error(__('eventmie-pro::em.event').' '.__('eventmie-pro::em.not_found'), Response::HTTP_BAD_REQUEST );
        
        return response([
            'myevents'=> $myevents->jsonSerialize(),
        ], Response::HTTP_OK);

    }

   
    
    // check login user role
    protected function is_admin(Request $request)
    {
        // if login user is Organiser then 
        // organiser id = Auth::id();
        $this->organiser_id = Auth::id();

        // if admin is creating event
        // then user Auth::id() as $organiser_id
        // and organiser id will be the id selected from Vue dropdown
        if(Auth::user()->hasRole('admin'))
        {
            $request->validate([
                'organiser_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            ], [
                'organiser_id.*' => __('eventmie-pro::em.organiser').' '.__('eventmie-pro::em.required'),
            ]);
            $this->organiser_id = $request->organiser_id;
        }
    }


    /**
     * Create-edit event
     *
     * @return array
     */
    public function form($slug = null)
    {
        
        $event  = [];
        
        // get event by event_slug
        if($slug)
        {
            $event = $this->event->get_event($slug);
            // user can't edit other user event but only admin can edit event's other users
            if(!Auth::user()->hasRole('admin') && Auth::id() != $event->user_id)
                return redirect()->route('eventmie.events_index');
        }
    
        $organisers = [];
        // fetch organisers dropdown
        // only if login user is admin
        if(Auth::user()->hasRole('admin'))
        {
            // fetch organisers
            $organisers    = $this->event->get_organisers();

            if($slug)
            {
                // in case of edit event, organiser_id won't change
                $this->organiser_id = $event->user_id;    
            }
        }

        $organiser_id    = $this->organiser_id ? $this->organiser_id : 0;

        return Eventmie::view('eventmie::events.form', compact('event', 'organisers', 'organiser_id'));
    }

    // create event
    public function store(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);
        
        // 1. validate data
        $request->validate([
            'title'             => 'required|max:256',
            'slug'              => 'required|max:512',
            'category_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            'description'       => 'required',
            'faq'               => 'required',
        ], [
            'category_id.*' => __('eventmie-pro::em.category').' '.__('eventmie-pro::em.required')
        ]);

        
        $result             = (object) [];
        $result->title      = null;
        
        // in case of edit
        if(!empty($request->event_id))
        {
            $request->validate([
                'event_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            ]);

            // check this event id have login user relationship
            $result      = (object) $this->event->get_user_event($request->event_id, $this->organiser_id);
        
            if(empty($result))
                return error('access denied', Response::HTTP_BAD_REQUEST );
    
        }
        
        // title is not equal to before title then apply unique column rule    
        if($result->title != $request->title)
        {
            $request->validate([
                'title'             => 'unique:events,title',
                'slug'              => 'unique:events,slug'
            ]);
        }

        $params = [
            "title"         => $request->title,
            "slug"          => $request->slug,
            "description"   => $request->description,
            "faq"           => $request->faq,
            "category_id"   => $request->category_id,
            "featured"      => 0,
        ];

        //featured
        if(!empty($request->featured))
        {
            $request->validate([
                'featured'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            ]);

            $params["featured"]       = $request->featured;
        }

        // set status to 0 only in case of admin
        // default event status is 1
        $params["status"]       = 1;
        if(Auth::user()->hasRole('admin'))
        {
            $status             = (int) $request->status;
            $params["status"]   = $status ? 1 : 0;
        }
        
        // only at the time of event create
        if(!$request->event_id)
        {
            $params["user_id"]       = $this->organiser_id;
            $params["item_sku"]      = time().rand(1,988);
        }
        
        $event    = $this->event->save_event($params, $request->event_id);
        
        if(empty($event))
            return error(__('eventmie-pro::em.event').' '.__('eventmie-pro::em.could_not').' '.__('eventmie-pro::em.created'), Response::HTTP_BAD_REQUEST );

        // ====================== Notification ====================== 
        //send notification after bookings
        $mail['mail_subject']   = __('eventmie-pro::em.event').' '.__('eventmie-pro::em.created').' '.__('eventmie-pro::em.successfully');
        $mail['mail_message']   = __('eventmie-pro::em.manage').' '.__('eventmie-pro::em.events');
        $mail['action_title']   = __('eventmie-pro::em.view').' '.__('eventmie-pro::em.my').' '.__('eventmie-pro::em.events');
        $mail['action_url']     = route('eventmie.myevents_index');
        $mail['n_type']       = "events";

        $notification_ids       = [1, $this->organiser_id];
        
        $users = User::whereIn('id', $notification_ids)->get();
        \Notification::send($users, new MailNotification($mail));
        // ====================== Notification ======================     
        
        
        // in case of create
        if(empty($request->event_id))
        {
            // set step complete
            $this->complete_step($event->is_publishable, 'detail', $event->id);
            return response()->json(['status' => true, 'id' => $event->id, 'organiser_id' => $event->user_id , 'slug' => $event->slug ]);
        }    
        // update event in case of edit
        $event      = $this->event->get_user_event($request->event_id, $this->organiser_id);
        return response()->json(['status' => true, 'slug' => $event->slug]);    
    }

    // complete specific step
    protected function complete_step($is_publishable = [], $type = 'detail', $event_id = null)
    {
        if(!empty($is_publishable))
            $is_publishable             = json_decode($is_publishable, true);

        $is_publishable[$type]      = 1;
        
        // save is_publishable
        $params     = ['is_publishable' => json_encode($is_publishable)];
        $status     = $this->event->save_event($params, $event_id);

        return true;
    }

    // crate media of event
    public function store_media(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        $images    = [];
        $poster    = null;
        $thumbnail = null;

        // 1. validate data
        $request->validate([
            'event_id'      => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            'thumbnail'     => 'required',
            'poster'        => 'required',
        ]);

        // vedio link optional so if have vedio ling then validation apply
        if(!empty($request->video_link))
        {
            $request->validate([
                'video_link'    => 'required',
            ]);
        }
        
        $result              = [];
        // check this event id have login user or not
        $result    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        if(empty($result))
        {
            return error('access denied', Response::HTTP_BAD_REQUEST );
        }
    
        // for multiple image
        $path = 'events/'.Carbon::now()->format('FY').'/';

        // for thumbnail
        if(!empty($_REQUEST['thumbnail'])) 
        { 
            $params  = [
                'image'  => $_REQUEST['thumbnail'],
                'path'   => 'events',
                'width'  => 512,
                'height' => 512,  
            ];
            $thumbnail   = $this->upload_base64_image($params);
        }

        if(!empty($_REQUEST['poster'])) 
        {
            $params  = [
                'image'  => $_REQUEST['poster'],
                'path'   => 'events',
                'width'  => 1920,
                'height' => 1080,  
            ];

            $poster   = $this->upload_base64_image($params);
        }
    
        // for image
        if($request->hasfile('images')) 
        { 
            // if have  image and database have images no images this event then apply this rule 
            $request->validate([
                'images'        => 'required',
                'images.*'      => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]); 
        
            $files = $request->file('images');
    
            foreach($files as  $key => $file)
            {
                $extension       = $file->getClientOriginalExtension(); // getting image extension
                $image[$key]     = time().rand(1,988).'.'.$extension;
                $file->storeAs('public/'.$path, $image[$key]);
                
                $images[$key]    = $path.$image[$key];
            }
        }
        
        $params = [
            "thumbnail"     => !empty($thumbnail) ? $path.$thumbnail : null ,
            "poster"        => !empty($poster) ? $path.$poster : null,
            "video_link"    => $request->video_link,
            "user_id"       => $this->organiser_id,
        ];  

        // if images uploaded
        if(!empty($images))
            $params["images"] = json_encode($images);
            
        
        $status   = $this->event->save_event($params, $request->event_id);

        if(empty($status))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }

        // get media  related event_id who have created now
        $images   = $this->event->get_user_event($request->event_id, $this->organiser_id);

        // set step complete
        $this->complete_step($images->is_publishable, 'media', $request->event_id);

        return response()->json(['images' => $images, 'status' => true]);
    }

    // crate location of event
    public function store_location(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        // 1. validate data
        $request->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'venue'             => 'required|max:256',
            'country_id'        => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            'address'           => 'required|max:512',
            'city'              => 'required|max:256',
            'state'             => 'required|max:256',
            'zipcode'           => 'required|max:64',
            'event_id'          => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            'latitude'          => ['nullable','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude'         => ['nullable','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
        ]);

        $params = [
            "venue"         => $request->venue,
            "address"       => $request->address,
            "city"          => $request->city,
            "zipcode"       => $request->zipcode,
            "country_id"    => $request->country_id,
            "state"         => $request->state,
            "latitude"      => $request->latitude,
            "longitude"     => $request->longitude,
        ];

        // only at the time of event create
        if(!$request->event_id)
        {
            $params["user_id"]       = $this->organiser_id;
        }

        // check this event id have login user or not
        $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        if(empty($check_event))
        {
            return error('access denied', Response::HTTP_BAD_REQUEST );
        }

        $location   = $this->event->save_event($params, $request->event_id);
        
        if(empty($location))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }

        // get update event
        $event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        // set step complete
        $this->complete_step($event->is_publishable, 'location', $request->event_id);
        
        return response()->json(['status' => true, 'event' => $event]);
    }    

    // crate timing of event
    public function store_timing(Request $request)
    {   
        // if logged in user is admin
        $this->is_admin($request);

        $request->validate([
            'event_id'          => 'required',
        ]); 
        
        $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        if(empty($check_event))
        {
            return error('access denied!', Response::HTTP_BAD_REQUEST );
        }

        $data           = [];
        $schedules      = [];

        $repetitive     = (int) $request->repetitive;
        
        if($repetitive)
        {
            $repetitive_event   = $this->prepare_repetitive_event($request);
            
            //=======================Update schedule case=============================================
            
            // update schedule true then return response to vue 
            if(!empty($repetitive_event['update']))
            {
                if($repetitive_event['update_schedule'] == true) 
                    return response()->json(['status' => true]);

                
                if($repetitive_event['update_schedule'] == false) 
                {
                    $msg = __('eventmie-pro::em.schedules').' '.__('eventmie-pro::em.could_not').' '.__('eventmie-pro::em.updated');
                    return error($msg, Response::HTTP_BAD_REQUEST );
                }
            }        
            //=====================================End==============================================    

                
            if(!$repetitive_event['status'])
                return error($repetitive_event['error'], Response::HTTP_BAD_REQUEST );
            
            $data               = $repetitive_event['repetitive_event']['data'];
            $schedules          = $repetitive_event['repetitive_event']['schedules'];
        }
        else
        {
            $single_event       = $this->prepare_single_event($request);

            if(!$single_event['status'])
                return error($single_event['error'], Response::HTTP_BAD_REQUEST );

            //======================Delete schedules if have any schedules in case of single repetitive event============================            
                
                // if changing event from repetitive to normal, then delete all schedules
                if($check_event->repetitive)
                {
                    $params  = [
                        'event_id'          => $request->event_id,
                        'user_id'           => $this->organiser_id,
                    ];
                    
                    // delete old schedule because changeid_date have true
                    $delete_schedule  = $this->schedule->delete_schedule($params); 
                    
                    if(empty($delete_schedule))
                    {
                        $msg = __('eventmie-pro::em.schedules').' '.__('eventmie-pro::em.could_not').' '.__('eventmie-pro::em.updated');
                        return error($msg, Response::HTTP_BAD_REQUEST );
                    }
                       
                }    
            //===========================End========================================================================    

            $data               = $single_event['data'];
        }
        
        $event_timing           = $this->event->save_event($data, $request->event_id);

        if(empty($event_timing))
            return error('Database failure!', Response::HTTP_BAD_REQUEST);


        // in case of repetitive event
        if(!empty($schedules))
        {
            // create repetitive event  schedule 
            $schedule      =  $this->schedule->create_schedule($schedules, $request->event_id);
            
            if(empty($schedule))
            {
                $msg = __('eventmie-pro::em.schedules').' '.__('eventmie-pro::em.could_not').' '.__('eventmie-pro::em.updated');
                return error($msg, Response::HTTP_BAD_REQUEST );
            }
        }
        
        // get update event
        $event    = $this->event->get_user_event($request->event_id, $this->organiser_id);
        // set step complete
        $this->complete_step($event->is_publishable, 'timing', $request->event_id);

        return response()->json(['status' => true]);
    }

    // add sponsores and speakers to event
    public function store_sponsors_speakers(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        $request->validate([
            'event_id'        => 'required',
            'speakers_ids'    => 'required',
        ]);
        
        // check event is valid or not
        $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        if(empty($check_event))
        {
            return error('access denied!', Response::HTTP_BAD_REQUEST );
        }

        $event_speakers     = [];
        $event_sponsors     = [];
    
        //===============Start Speakers ==================================================================    
     
        // comma value to conver to array for speakers
        $speakers_ids       = explode(',', $request->speakers_ids);
        
        // check speaker and login user relationship 
        $speakers           = Speaker::whereIn('id', $speakers_ids)
                                    ->where(['user_id' => $this->organiser_id])
                                    ->get()
                                    ->toArray();
        if(empty($speakers))
        {
            $msg = __('eventmie-pro::em.speaker').' '.__('eventmie-pro::em.not_found');
            return response()->json(['status' => false, 'message' => $msg ]);
        }

        foreach($speakers as $key => $value)
        {
            $event_speakers[$key]['event_id']    = $request->event_id;
            $event_speakers[$key]['speaker_id']  = $value['id'];
        }
        //======================================End Speakers===========================================================    
       

        //====================================Start Sponsors===========================================================
        
        // sponsors ara optional so check sponsors empty or not 
        if(!empty($request->sponsors_ids))
        {
            // comma value to conver to array for speakers
            $sponsors_ids       = explode(',', $request->sponsors_ids);
            
            // check speaker and login user relationship 
            $sponsors           = Sponsor::whereIn('id', $sponsors_ids)
                                        ->where(['user_id' => $this->organiser_id])
                                        ->get()
                                        ->toArray();
            if(empty($sponsors))
            {
                $msg = __('eventmie-pro::em.sponsor').' '.__('eventmie-pro::em.not_found');
                return response()->json(['status' => false, 'message' => $msg]);
            }

            foreach($sponsors as $key => $value)
            {
                $event_sponsors[$key]['event_id']     = $request->event_id;
                $event_sponsors[$key]['sponsor_id']   = $value['id'];
            }
            
        }
        //=============================End Sponsors=============================================================    
        

        $params = [
            'event_speakers' => $event_speakers,
            'event_sponsors' => $event_sponsors,
            "event_id"       => $request->event_id,
            "user_id"        => $this->organiser_id,
        ];
        
        
       $status = $this->event->sponsors_speakers($params);

       
        // get update event
        $event    = $this->event->get_user_event($request->event_id, $this->organiser_id);
        
        

       // both are inserted successfully
        if($status['event_speaker'])
        {
            // set step complete
            $this->complete_step($event->is_publishable, 'poweredby', $request->event_id);
            $msg = __('eventmie-pro::em.event').' '.__('eventmie-pro::em.updated').' '.__('eventmie-pro::em.successfully');
            return response()->json(['status' => true, 'message' => $msg ]);
        }
        
        return response()->json(['status' => false, 'message' => 'Database failure!' ]);

    }

    
    // store seo for event
    public function store_seo(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        // 1. validate data
        $request->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'meta_title'             => 'required|max:256',
            'meta_keywords'          => 'max:256',
            'meta_description'       => 'required|max:512',
        ]);

        $params = [
            "meta_title"         => $request->meta_title,
            "meta_keywords"      => $request->meta_keywords,
            "meta_description"   => $request->meta_description,
          
        ];

        
        // check this event id have login user or not
        $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        if(empty($check_event))
        {
            return error('access denied', Response::HTTP_BAD_REQUEST );
        }

        $seo   = $this->event->save_event($params, $request->event_id);
        
        if(empty($seo))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }

        // get update event
        $event    = $this->event->get_user_event($request->event_id, $this->organiser_id);
        
        return response()->json(['status' => true, 'event' => $event]);
    }   

    // publish event
    public function event_publish(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        $request->validate([
            'event_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
        ]);
        
        // check event is valid or not
        $event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        if(empty($event))
        {
            return error('access denied!', Response::HTTP_BAD_REQUEST );
        }

        // check all step is completed or not 
        $is_publishable     =  json_decode($event->is_publishable, true);

        if(count($is_publishable) != 6)
            return error(__('eventmie-pro::em.please_complete_steps'), Response::HTTP_BAD_REQUEST );

            if(config('voyager.demo_mode'))
            {
                if($event->publish)
                    return error('Demo mode', Response::HTTP_BAD_REQUEST );
            }
        
        $params  = [
            'publish'      => $event->publish == 1 ? 0 : 1,
        ];

        $publish_event    = $this->event->save_event($params, $request->event_id);

        if(empty($publish_event))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );
        }

        return response()->json(['status' => true ]);

    }

    // get event
    public function get_user_event(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        $request->validate([
            'event_id'        => 'required',
            
        ]);
        
        // check event is valid or not
        $event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        if(empty($event))
        {
            return error('access denied!', Response::HTTP_BAD_REQUEST );
        }

        return response()->json(['status' => true, 'event' => $event ]);

    }

      
    
    // get all countries
    public function countries()
    {
        $countries = $this->country->get_countries();

        if(empty($countries))
        {
            return response()->json(['status' => false]);    
        }
        return response()->json(['status' => true, 'countries' => $countries ]);
        
    }

    /**
     *   organiser can disable own event
     */

    public function disable_event(Request $request)
    {
        
        // validation 
        $request->validate([
            'event_id'          => 'required',
        ]); 

        // organiser id
        $this->organiser_id     = Auth::id();

        // check event exist or not base on event_id and organiser_id
        $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        if(empty($check_event))
        {
            return error('access denied!', Response::HTTP_BAD_REQUEST );
        }

        $params    = [
           'organiser_id' => Auth::id(),
            'event_id'     => $request->event_id,
        ];

        $data = [
            'status' => $check_event->status == 1 ?  0 : 1,
        ];

        $delete_event     = $this->event->disable_event($params, $data);

        if(empty($delete_event))
        {
            return error('Database failure!', Response::HTTP_BAD_REQUEST );   
        }
        
        // check that this event have any schedules or not if schedule then delete schedule
        if($check_event->repetitive > 0 )
        {
            $delete_event_schedules    = $this->schedule->disable_event_schedules($params);

            if(empty($delete_event_schedules))
                return error('Database failure!', Response::HTTP_BAD_REQUEST );   
        }

        $msg = __('eventmie-pro::em.event').' '.__('eventmie-pro::em.disabled').' '.__('eventmie-pro::em.successfully');
        return response()->json(['status' => true, 'message' => $msg ]);
        
    }

    /**
     *   only admin can delete event
     */

    public function delete_event($slug = null)
    {   
        if(config('voyager.demo_mode'))
        {
            return redirect()
            ->route("voyager.events.index")
            ->with([
                'message'    => 'Demo mode',
                'alert-type' => 'info',
            ]);
        }
        
        // only admin can delete event
        
        if(Auth::check() && !Auth::user()->hasRole('admin'))
        {
            return redirect()->route('eventmie.events');
        }

        // get event by event_slug
        if(empty($slug))
            return error('Event Not Found!', Response::HTTP_BAD_REQUEST );
        

        $event = $this->event->get_event($slug);
        
        if(empty($event))
            return error('Event Not Found!', Response::HTTP_BAD_REQUEST );

        $params    = [
            'event_id'     => $event->id,
        ];

        $delete_event     = $this->event->delete_event($params);

        if(empty($delete_event))
        {
            return error('Event Could Not Deleted!', Response::HTTP_BAD_REQUEST );   
        }

        $msg = __('eventmie-pro::em.event').' '.__('eventmie-pro::em.deleted').' '.__('eventmie-pro::em.successfully');
        
        return redirect()
        ->route("voyager.events.index")
        ->with([
            'message'    => $msg,
            'alert-type' => 'success',
        ]); 
        
    }

    /**
     * export_attendees
     */

    public function export_attendees($slug)
    {
        // check event is valid or not
        $event    = $this->event->get_event($slug);
        if(empty($event))
        {
            return error('access denied!', Response::HTTP_BAD_REQUEST );
        }
        
        $params   = [
            'event_id' => $event->id,
        ];

        // get particular event's bookings
        $bookings = $this->booking->get_event_bookings($params);
        if(empty($bookings))
            return error_redirect('Booking Not Found!');

        // convert array to collection for csv
        $bookings = collect($bookings);

        // create object of laracsv
        $csvExporter = new \Laracsv\Export();
    
        // create csv 
        $csvExporter->build($bookings, [
            
            //events fields which will be include
            'id',
            'itm_sku',
            'event_category',
            'event_title',
            'order_number',
            'customer_name', 
            'customer_email', 
            'quantity', 
            'price',
            'tax',
            'net_price',
            'event_start_date',
            'event_end_date',
            'event_start_time',
            'event_end_time',
            'ticket_title',
            'ticket_price',
            'currency',
            'created_at', 
            'updated_at'
        ]);
        
        // download csv
        $csvExporter->download($event->slug.'-attendies.csv');
    } 
    

    /* ==================== Private fucntions ==================== */
    
    /**
     *  Upload base64 image 
     */
    protected function upload_base64_image($params = [])
    {
        if(!empty($params['image'])) 
        { 
            $image           = base64_encode(file_get_contents($params['image']));
            $image           = str_replace('data:image/png;base64,', '', $image);
            $image           = str_replace(' ', '+', $image);

            if(class_exists('\Str'))
                $filename        = time().\Str::random(10).'.'.'jpg';
            else
                $filename        = time().str_random(10).'.'.'jpg';
            
            $path            = '/storage/'.$params['path'].'/'.Carbon::now()->format('FY').'/';
            $image_resize    = Image::make(base64_decode($image))->resize($params['width'], $params['height']);

            // first check if directory exists or not
            
            if (! File::exists(public_path().$path)) {
                File::makeDirectory(public_path().$path);
            }
    
            $image_resize->save(public_path($path . $filename));
            
            return  $filename;
        }
    } 

    /**
     *  prepare_single_event
     */

    protected function prepare_single_event(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        // 1. validate data
        $request->validate([
            'start_date'        => 'required|date|after_or_equal:tomorrow',
            'end_date'          => 'required|date|after_or_equal:start_date',
            'start_time'        => 'required|date_format:H:i:s',
            'end_time'          => 'required|date_format:H:i:s',
        ]);

        $data = [
            "start_date"        => $request->start_date,
            "start_time"        => $request->start_time,
            "end_date"          => $request->end_date,
            "end_time"          => $request->end_time,
            "repetitive"        => $request->repetitive,
        ];

        return [
            'status'    => true, 
            'data'      => $data
        ];
    }

    /**
     * prepare_repetitive_event
     */
   
    protected function prepare_repetitive_event(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        $request->validate([
            'start_date'        => 'required|date|after_or_equal:tomorrow',
            'end_date'          => 'required|date|after_or_equal:start_date',
            'start_time'        => 'required|date_format:H:i:s',
            'end_time'          => 'required|date_format:H:i:s',
            'repetitive'        => 'required|numeric|between:1,1',
            'repetitive_type'   => 'required|numeric|between:1,3',
        ]);

        // 1. count_months => start_date & end_date
        // 2. months_dates => start_date & end_date
            // $months_dates[0]['from_date'] 
            // $months_dates[0]['to_date'] 

            // $months_dates[1]['from_date'] 
            // $months_dates[2]['to_date'] 

        // 3. count_months == count(repetitive_dates) == count(from_time) == count(to_time)
            // return error('Error message!', Response::HTTP_BAD_REQUEST );
        
        $start_month               = \Carbon\Carbon::createFromFormat('Y-m-d', $request->start_date)->modify('first day of this month');
        $end_month                 = \Carbon\Carbon::createFromFormat('Y-m-d', $request->end_date)->modify('last day of this month');
        $count_months              = $end_month->diffInMonths($start_month)+1;

        // validate from_time and to_time
        $request->validate([
            'from_time'             => ['required', 'array', "min:$count_months", "max:$count_months"],
            'from_time.*'           => ['required', 'date_format:H:i:s'],

            'to_time'               => ['required', 'array', "min:$count_months", "max:$count_months"],
            'to_time.*'             => ['required', 'date_format:H:i:s', 'after:from_time.*'],
        ]);

        $repetitive_type = (int) $request->repetitive_type;

        // count_months == count_repetitive_dates
        // repetitive dates
        if( ($repetitive_type === 1 || $repetitive_type === 3) && !empty($request->repetitive_dates))
        {
            $request->validate([
                'repetitive_dates'       => ['required', 'array', "min:$count_months", "max:$count_months"],
                'repetitive_dates.*'     => ['required', 'regex:/^[0-9 ,]+$/i'], 
            ]);
        }
            
        // count_months == count_repetitive_days
        // repetitive days
        if( ($repetitive_type === 2) && !empty($request->repetitive_days))
        {
            $request->validate([
                'repetitive_days'       => ['required', 'array', "min:$count_months", "max:$count_months"],
                'repetitive_days.*'     => ['required', 'regex:/^[0-9 ,]+$/i'], 
            ]);
        }


        $start_date     = \Carbon\Carbon::createFromFormat('Y-m-d', $request->start_date)->modify('first day of this month');
        $end_date       = \Carbon\Carbon::createFromFormat('Y-m-d', $request->end_date)->modify('last day of this month');
        
        // get months name between two dates
        $month_names_temp = CarbonPeriod::create($start_date, '1 month', $end_date);
        

        $month_names     = [];
        $from_date       = [];
        $to_date         = [];
        
        foreach ($month_names_temp as $key => $value) 
        {
            $month_names[$key] = $value->format("Y-m-d") ;
        }
        
        if(!empty($month_names))
        {
            // get first day of months
            foreach ($month_names as $key => $value) {
                $first_day_month   = new \DateTime($value);
                $first_day_month->modify('first day of this month');
                $from_date[$key]   = $first_day_month->format('Y-m-d');
            }

            // get last day of months
            foreach ($month_names as $key => $value) {
                $last_day_month  = new \DateTime($value);
                $last_day_month->modify('last day of this month');
                $to_date[$key]   = $last_day_month->format('Y-m-d');
            }
        }
    
        $schedules           = [];

        // current date time
        $current_date_time   = Carbon::now()->toDateTimeString();
        
        // repetitive dates
        if( ($repetitive_type === 1 || $repetitive_type === 3) && !empty($request->repetitive_dates))
        {
            foreach ($request->repetitive_dates as $key => $value)
            {
                $schedules[$key]['event_id']             = $request->event_id;
                $schedules[$key]['user_id']              = $this->organiser_id;
                $schedules[$key]['repetitive_type']      = $repetitive_type;
                $schedules[$key]['repetitive_dates']     = $value;
                $schedules[$key]['from_time']            = $request->from_time[$key];
                $schedules[$key]['to_time']              = $request->to_time[$key];

                // generate from start_date and end_date
                $schedules[$key]['from_date']            = $from_date[$key];
                $schedules[$key]['to_date']              = $to_date[$key];
                $schedules[$key]['created_at']           = $current_date_time;
                $schedules[$key]['updated_at']           = $current_date_time;

                // timestamp
                // make repetitive_days = null
                $schedules[$key]['repetitive_days']      = null;
            }
        }    

        // repetitive days
        if( ($repetitive_type === 2) && !empty($request->repetitive_days))
        {
            foreach ($request->repetitive_days as $key => $value)
            {
                $schedules[$key]['event_id']             = $request->event_id;
                $schedules[$key]['user_id']              = $this->organiser_id;
                $schedules[$key]['repetitive_type']      = $repetitive_type;
                $schedules[$key]['repetitive_days']      = $value;
                $schedules[$key]['from_time']            = $request->from_time[$key];
                $schedules[$key]['to_time']              = $request->to_time[$key];

                // generate from start_date and end_date
                $schedules[$key]['from_date']            = $from_date[$key];
                $schedules[$key]['to_date']              = $to_date[$key];
                $schedules[$key]['created_at']           = $current_date_time;
                $schedules[$key]['updated_at']           = $current_date_time;

                // make repetitive_dates = null
                $schedules[$key]['repetitive_dates']      = null;
            }
        }    
        
        $data = [
            "start_date"        => $request->start_date,
            "start_time"        => $request->start_time,
            "end_date"          => $request->end_date,
            "end_time"          => $request->end_time,
            "repetitive"        => $request->repetitive,
            "merge_schedule"    => $repetitive_type > 1 ? $request->merge_schedule : 0,
            "user_id"           => $this->organiser_id,
        ];


        $repetitive_event = [
            'data'      => $data,
            'schedules' => $schedules,
        ];

        // check for update schedule
            // 1. Check if there's any schedule for this event (call model function)
            // if yes
            // -----------------------
                // 1. check if start_date and end_date changed (if)
                    // if dates changed then delete all schedules
                    // then continue below in this method
                
                // 2. if start_date and end_date not changed (else)
                // call this->update_schedule

            // -----------------------
            // if no 
            // then continue

            // if(already_schedules_available)
            // {
                // 
                    // if(check if dates are changed)
                    // {
                            // delete all schedules
                    // }
                    // else
                    // {
                            // call this->update_schedule
                            // return success;
                    // }
            // }
            // get old event
            $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);
            // check schedule ids empty on not
            if(!empty($request->schedule_ids))
            {
                // validate from_time and to_time
                $request->validate([
                    'schedule_ids'          => ['required', 'array'],
                    'schedule_ids.*'        => ['required'],
                ]);

                $params  = [
                    'schedule_ids'      => $request->schedule_ids,
                    'event_id'          => $request->event_id,
                    'repetitive_type'   => $request->repetitive_type,
                    'user_id'           => $this->organiser_id,
                ];
                
                
                // check schedule ids existed on not in schedule table
                $check_schedule     = $this->schedule->check_schedule($params);

                if(!empty($check_schedule))
                {
                    $old_start_date     = Carbon::createFromFormat('Y-m-d', $check_event['start_date']);
                    $old_end_date       = Carbon::createFromFormat('Y-m-d', $check_event['end_date']);

                    $new_start_date     = Carbon::createFromFormat('Y-m-d', $request->start_date);
                    $new_end_date       = Carbon::createFromFormat('Y-m-d', $request->end_date);

                    // if event date have not changed to previous dates then change_date variable false 
                    $changed_date       = true;
                    if($old_start_date->equalTo($new_start_date) && $old_end_date->equalTo($new_end_date))
                    {
                        
                        $changed_date    = false;

                        if($request->merge_schedule != $check_event->merge_schedule)
                            $changed_date    = true;
                    }

                    // if changed_date true means dates have changed then old schedule delete then new create schedule
                    if($changed_date)
                    {
                        $params  = [
                            'event_id'          => $request->event_id,
                            'user_id'           => $this->organiser_id,
                        ];
                        
                        // delete old schedule because changeid_date have true
                        $delete_schedule  = $this->schedule->delete_schedule($params);

                        if(empty($delete_schedule))
                        {
                            $msg = __('eventmie-pro::em.schedules').' '.__('eventmie-pro::em.could_not').' '.__('eventmie-pro::em.updated');
                            return error($msg, Response::HTTP_BAD_REQUEST );
                        }

                        $params = [
                            'repetitive' => 0
                        ];
                        // update repetitive column by 0 after schedule deleted
                        $this->event->save_event($params, $request->event_id);

                    }
                    else
                    {
                        $update_schedule  = $this->update_schedule($request, $schedules);

                        if(!$update_schedule['status'])
                                return ['update' => 1, 'update_schedule' => false];;
                        
                        return ['update' => 1, 'update_schedule' => true];
                    }
                }
                else
                {
                    $msg = __('eventmie-pro::em.schedules').' '.__('eventmie-pro::em.could_not').' '.__('eventmie-pro::em.updated');
                    return error($msg, Response::HTTP_BAD_REQUEST );
                }
            }

            $params  = [
                'event_id'          => $request->event_id,
                'user_id'           => $this->organiser_id,
            ];
            
            // delete old schedule 
            $delete_schedule  = $this->schedule->delete_schedule($params);

        return [
            'status'            => true,
            'repetitive_event'  => $repetitive_event,
        ]; 
        
    }

    // update repetitive event schedule
    protected function update_schedule(Request $request, $schedules = [])
    {
        // if logged in user is admin
        $this->is_admin($request);

        // if changed_date false means dates have not changed then update schedule
        
        if(empty($schedules))
        {
            $msg = __('eventmie-pro::em.schedules').' '.__('eventmie-pro::em.could_not').' '.__('eventmie-pro::em.updated');
            return error($msg, Response::HTTP_BAD_REQUEST );
        }
        
        $params       = [
            'schedules'     => $schedules,
            'schedule_ids'  => $request->schedule_ids,
            'event_id'      => $request->event_id,
            'user_id'       => $this->organiser_id,
        ];

        // in case of repetitive event
        if(!empty($params))
        {
            $update_schedule      =  $this->schedule->update_schedule($params);

            if(empty($update_schedule))
            {
                $msg = __('eventmie-pro::em.schedules').' '.__('eventmie-pro::em.could_not').' '.__('eventmie-pro::em.updated');
                return ['status' => false, 'error' => $msg];
            }
            return ['status' => true];
        }

        $msg = __('eventmie-pro::em.invalid').' '.__('eventmie-pro::em.data');
        return ['status' => false, 'error' => $msg];
    }
}