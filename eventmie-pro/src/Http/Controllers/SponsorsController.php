<?php

namespace Classiebit\Eventmie\Http\Controllers;
use App\Http\Controllers\Controller; 
use Facades\Classiebit\Eventmie\Eventmie;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use File;

use Carbon\Carbon;

use Auth;


use Classiebit\Eventmie\Models\Sponsor;
use Classiebit\Eventmie\Models\Event;

class SponsorsController extends Controller
{

    public function __construct()
    {
        // language change
        $this->middleware('common');
    
        //except means exculde
        $this->middleware(['organiser', 'admin'])->except(['sponsors', 'selected_event_sponsors']);
        $this->sponsor      = new Sponsor;
        $this->event        = new Event;
        $this->organiser_id  = null;   
    }

    /**
     * Create-edit sponsors
     *
     * @return array
     */
    public function form($id = null)
    {
        // get prifex from eventmie config
        $path = false;
        if(!empty(config('eventmie.route.prefix')))
            $path = config('eventmie.route.prefix');

        $id     = (int) $id;
        $event  = [];
        
        $organisers = [];
        // fetch organisers dropdown
        // only if login user is admin
        if(Auth::user()->hasRole('admin'))
        {
            // fetch organisers
            $organisers    = $this->event->get_organisers();

            if($id)
            {
                // in case of edit event, organiser_id won't change
                $this->organiser_id = $event->user_id;    
            }
        }

        $organiser_id    = $this->organiser_id ? $this->organiser_id : 0;
        
        return Eventmie::view('eventmie::sponsors.form', compact('organisers', 'organiser_id', 'path'));
    }

    /**
     * fetch sponsors
     *
     **/
    public function sponsors(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        $params  = [
            'user_id'  => $this->organiser_id,
        ];

        $sponsors  = $this->sponsor->get_sponsors($params);
        
        return response([
            'sponsors'=> $sponsors->jsonSerialize(),
            'user_id' => Auth::id(),
        ], Response::HTTP_OK);
    }

    // add sponsors
    public function store(Request $request)
    {
         // if logged in user is admin
        $this->is_admin($request);

        // 1. validate data
        $request->validate([
            'name'    => 'required|string|max:256',
            'website' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        ]);

        $image      = null;
        
        
        // in case of edit
        if(!empty($request->sponsor_id))
        {
            $sponsor  = $this->sponsor->get_sponsor($request->sponsor_id);
            
            if(!empty($sponsor))
                $image    = $sponsor->image;
        }
        
        $path = 'sponsors/'.Carbon::now()->format('FY').'/';

        // for image
        if($request->hasfile('image')) 
        { 
            // if have  image and database have images no images this event then apply this rule 
            $request->validate([
                'image'        => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]); 

            $file      = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting poster extension
            $image     = time().rand(1,988).'.'.$extension;
            // $file->storeAs('public/'.$path, $image);
            $image_resize    = Image::make($file)->resize(512,512);

            // if directory not exist then create directiory
            if (! File::exists(storage_path('/app/public/').$path)) {
                File::makeDirectory(storage_path('/app/public/').$path);
            }
            
            $image_resize->save(storage_path('/app/public/'.$path.$image));
            
            $image     =  $path.$image;
        }

        if(empty($image))
        {
            // if have  image and database have images no images this event then apply this rule 
            $request->validate([
                'image'        => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]); 
        }

        $params   = [
            'name'          => $request->name,
            'website'       => $request->website,
            'image'         => $image,
            'user_id'       => $this->organiser_id,
            'status'        => 1,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ];
        
        $sponsor  = $this->sponsor->add_sponsors($params, $request->sponsor_id);

        if(empty($sponsor))
        {
            // this is response-helpers error() helper function
            return error('Database failure!', Response::HTTP_REQUEST_TIMEOUT);
        }
        
        $msg = __('eventmie-pro::em.sponsor').' '.__('eventmie-pro::em.saved').' '.__('eventmie-pro::em.successfully');
        return response()->json(['success' => $msg, 'data'=> $sponsor], 200);
    }

    // delete speakers
    public function delete(Request $request)
    {
         // if logged in user is admin
        $this->is_admin($request);
         
           // 1. validate data
        $request->validate([
            'sponsor_id'   => 'required',
        ]);

        $delete  = $this->sponsor->delete_sponsors($request->sponsor_id);

        if(empty($delete))
        {
            return response()->json(['status' => false]);    
        }
        return response()->json(['status' => true]);
    }

    //get selected sponsors from event_sponsors table when organiser editing his event
    public function selected_event_sponsors(Request $request)
    {
         // if logged in user is admin
        $this->is_admin($request);
         
           // 1. validate data
        $request->validate([
            'event_id'   => 'required',
        ]);

        // check event is valid or not
        $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        // if event not found then access denie!
        if(empty($check_event))
        {
            return error('access denied!', Response::HTTP_BAD_REQUEST );
        }

        // get seleced speaker
        $selected_event_sponsors   = $this->sponsor->selected_event_sponsors($request->event_id);

        if(empty($selected_event_sponsors))
        {
            return response()->json(['status' => false, 'selected_event_sponsors'=> $selected_event_sponsors], 200);
        }

        return response()->json(['status' => true, 'selected_event_sponsors'=> $selected_event_sponsors], 200);
    }

    // check admin or organiser
    protected function is_admin(Request $request)
    {
        // if login user is Organiser then 
        // organiser id = Auth::id();
        $this->organiser_id = Auth::id();

        // if admin is creating event
        // then user Auth::id() as $organiser_id
        // and organiser id will be the id selected from Vue dropdown
        if(Auth::user()->hasRole('admin'))
        {
            $request->validate([
                'organiser_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            ]);
            $this->organiser_id = $request->organiser_id;
        }
    }
}
