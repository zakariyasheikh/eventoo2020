<?php

namespace Classiebit\Eventmie\Http\Controllers\Auth;
use Facades\Classiebit\Eventmie\Eventmie;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Classiebit\Eventmie\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;

use Classiebit\Eventmie\Notifications\MailNotification;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {  
         // language change
        $this->middleware('common');
        $this->middleware('guest')->except('logout');
        $this->redirectTo = !empty(config('eventmie.route.prefix')) ? config('eventmie.route.prefix') : '/';
    }

    /**
     *  Handle Social login request
     *
     * @return response
    */
    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
    }

    /**
    *  Obtain the user information from Social Logged in.
    *  @param $social
    *  @return Response
    */
    public function handleProviderCallback($social)
    {
        try{
            $userSocial = Socialite::driver($social)->user();
        }
        catch(\Exception $e){
            return redirect()->route('eventmie.welcome');
        }
        
        // email is required
        if(empty($userSocial->getEmail()))
            return error_redirect([__('eventmie-pro::em.email').' '.__('eventmie-pro::em.required'), "There is no email attached to your ".ucfirst($social)." account."]);

        $user = User::where(['email' => $userSocial->getEmail()])->first();

        // if user with same email already exist then login 
        if($user)
        {
            \Auth::login($user);

            return redirect()->route('eventmie.welcome');
        }
        else
        {
            // else register the user first then login
            if(!empty($userSocial->getName()))
                $name   = $userSocial->getName();
            else
                $name   = ucfirst(strstr($userSocial->getEmail(), '@', true));

            $new_user = User::create([
                'name' => $name,
                'email' => $userSocial->getEmail(),
                'password' => Hash::make(rand(1,988)), // random password
                'role_id'  => 2,
            ]);
            
            $user = User::where(['email' => $userSocial->getEmail()])->first();

            \Auth::login($user);

            // Send welcome email
            if(!empty($new_user->email))
            {
                // ====================== Notification ====================== 
                $mail['mail_subject']   = __('eventmie-pro::em.congrats').' '.__('eventmie-pro::em.register').' '.__('eventmie-pro::em.successful');
                $mail['mail_message']   = __('eventmie-pro::em.get_tickets');
                $mail['action_title']   = __('eventmie-pro::em.login');
                $mail['action_url']     = eventmie_url();
                $mail['n_type']         = "user";

                // notification for
                $notification_ids       = [
                    1 // admin
                ];
                
                $users = User::whereIn('id', $notification_ids)->get();
                \Notification::send($users, new MailNotification($mail));
                // ====================== Notification ======================     
            }
            
            return redirect()->route('eventmie.welcome');
        }
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        if (\Auth::check()) {
            return redirect()->route('eventmie.welcome');
        }
        return Eventmie::view('eventmie::auth.login');
    }

    /**
     *  after login
     */

    // check if authenticated, then redirect to welcome page
    protected function authenticated() 
    {
        if (\Auth::check()) {
            return redirect()->route('eventmie.welcome');
        }
    }

    public function login(Request $request) 
    {
        $this->validate($request, [
            
            'email'    => 'required|email',
            'password' => 'required'
            
        ]);

        $flag = \Auth::attempt ([
            'email' => $request->get ( 'email' ),
            'password' => $request->get ( 'password' ) 
        ]);
        
        if ($flag) 
        {
            $redirect = !empty(config('eventmie.route.prefix')) ? config('eventmie.route.prefix') : '/';
            return redirect($redirect);
        } 
        else 
        {
            return error_redirect( __('eventmie-pro::em.login').' '.__('eventmie-pro::em.failed') );
        }
    }
    
}

