<?php

namespace Classiebit\Eventmie\Http\Controllers;
use App\Http\Controllers\Controller; 
use Facades\Classiebit\Eventmie\Eventmie;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use File;

use Auth;

use Classiebit\Eventmie\Models\Speaker;
use Classiebit\Eventmie\Models\Event;

class SpeakersController extends Controller
{
    
    public function __construct()
    {
        // language change
        $this->middleware('common');
    
        $this->middleware(['organiser', 'admin'])->except(['speakers', 'selected_event_speakers']);
        $this->speaker      = new Speaker;
        $this->event        = new Event;
        $this->organiser_id = null;   
    }

    /**
     * Create-edit speakers
     *
     * @return array
     */
    public function form($id = null)
    {
        // get prifex from eventmie config
        $path = false;
        if(!empty(config('eventmie.route.prefix')))
            $path = config('eventmie.route.prefix');

        $id     = (int) $id;
        $event  = [];
        
        $organisers = [];
        // fetch organisers dropdown
        // only if login user is admin
        if(Auth::user()->hasRole('admin'))
        {
            // fetch organisers
            $organisers    = $this->event->get_organisers();

            if($id)
            {
                // in case of edit event, organiser_id won't change
                $this->organiser_id = $event->user_id;    
            }
        }

        $organiser_id    = $this->organiser_id ? $this->organiser_id : 0;

        return Eventmie::view('eventmie::speakers.form', compact('organisers', 'organiser_id', 'path'));
    }

    // get speakers
    public function speakers(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);

        $params    = [
            'user_id' => $this->organiser_id,
        ];
        
        $speakers  = $this->speaker->get_speakers($params);

        if(empty($speakers))
        {
            return response()->json(['status' => false]);    
        }


        return response()->json(['status' => true, 'speakers' => $speakers->jsonSerialize() ]);
    }

    // add speakers
    public function store(Request $request)
    {
        // if logged in user is admin
        $this->is_admin($request);
        
        $image = null;
        // 1. validate data
        $request->validate([
            
            "fullname"       => 'required|max:256',
            "designation"    => 'required|max:256',
            "about"          => 'required',
            "phone"          => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            "email"          => 'nullable|email',
            "instagram"      => 'nullable|max:512',
            "facebook"       => 'nullable|max:512',
            "twitter"        => 'nullable|max:512',
            "linkedin"       => 'nullable|max:512',
            "website"        => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            
        ]);

        
        $image  = null;
        
        // in case of edit speaker
        if(!empty($request->fullname))
        {
            // get speaker base on speraker id
            $speaker     = $this->speaker->get_speaker($request->fullname);

            if(!empty($speaker))
                $image       = $speaker->image;   
        }

        $path = '/speakers/'.Carbon::now()->format('FY').'/';

        // for image
        if($request->hasfile('image')) 
        { 
            // image type validation 
            $request->validate([
                'image'        => 'image|mimes:jpeg,png,jpg',
            ]); 

            $file      = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting poster extension
            $image     = time().rand(1,988).'.'.$extension;
            // $file->storeAs('public/'.$path, $image);
            $image_resize    = Image::make($file)->resize(512,512);

            // if directory not exist then create directiory
            if (! File::exists(storage_path('/app/public/').$path)) {
                File::makeDirectory(storage_path('/app/public/').$path);
            }
            
            $image_resize->save(storage_path('/app/public/'.$path.$image));
            $image     = $path.$image;
        }
       
        // image is required
        if(empty($image))
        {
            // if have  image and database have images no images this event then apply this rule 
            $request->validate([
                'image'        => 'required|image|mimes:jpeg,png,jpg',
            ]); 
        }

        $params = [
            "fullname"       => $request->fullname,
            "designation"    => $request->designation,
            "about"          => $request->about,
            "phone"          => $request->phone,
            "email"          => $request->email,
            "instagram"      => $request->instagram,
            "facebook"       => $request->facebook,
            "twitter"        => $request->twitter,
            "linkedin"       => $request->linkedin,
            "website"        => $request->website,
            "image"          => $image,
            "user_id"        => $this->organiser_id,
        ];

        $speaker_id   = $request->speaker_id; 
    
        $speakers     = $this->speaker->add_speakers($params, $speaker_id);

        if(empty($speakers))
        {
            return response()->json(['status' => false]);    
        }
        return response()->json(['status' => true]);
    }

    // delete speakers
    public function delete(Request $request)
    {
         // if logged in user is admin
        $this->is_admin($request);
         
           // 1. validate data
        $request->validate([
            'speaker_id'   => 'required',
        ]);
        $total_speakers      = $this->speaker->total_speakers();

        // Organiser can't delete last speaker
        if($total_speakers <= 1)
        {
            return error(__('eventmie-pro::em.speaker').' '.__('eventmie-pro::em.required'), Response::HTTP_BAD_REQUEST );
        }

        $delete  = $this->speaker->delete_speakers($request->speaker_id);

        if(empty($delete))
        {
            return response()->json(['status' => false]);    
        }
        return response()->json(['status' => true]);
    }

    //get selected speakers from event_sponsors table when organiser editing his event
    public function selected_event_speakers(Request $request)
    {
         // if logged in user is admin
        $this->is_admin($request);
         
           // 1. validate data
        $request->validate([
            'event_id'   => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
        ]);

        // check event is valid or not
        $check_event    = $this->event->get_user_event($request->event_id, $this->organiser_id);

        // if event not found then access denie!
        if(empty($check_event))
        {
            return error('access denied!', Response::HTTP_BAD_REQUEST );
        }

        // get seleced speaker
        $selected_event_speakers   = $this->speaker->selected_event_speakers($request->event_id);

        if(empty($selected_event_speakers))
        {
            return response()->json(['status' => false, 'selected_event_speakers'=> $selected_event_speakers], 200);
        }

        return response()->json(['status' => true, 'selected_event_speakers'=> $selected_event_speakers], 200);
    }

    protected function is_admin(Request $request)
    {
        // if login user is Organiser then 
        // organiser id = Auth::id();
        $this->organiser_id = Auth::id();

        // if admin is creating event
        // then user Auth::id() as $organiser_id
        // and organiser id will be the id selected from Vue dropdown
        if(Auth::user()->hasRole('admin'))
        {
            $request->validate([
                'organiser_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            ]);
            
            $this->organiser_id = $request->organiser_id;
        }
    }

    
}
