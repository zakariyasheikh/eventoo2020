<?php

namespace Classiebit\Eventmie\Http\Controllers;
use App\Http\Controllers\Controller; 
use Facades\Classiebit\Eventmie\Eventmie;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Auth;
use Classiebit\Eventmie\Models\Event;
use Classiebit\Eventmie\Models\Ticket;
use Classiebit\Eventmie\Models\Booking;
use Classiebit\Eventmie\Models\Transaction;
use Classiebit\Eventmie\Models\Commission;
use Classiebit\Eventmie\Models\User;
use Classiebit\Eventmie\Notifications\MailNotification;



class OBookingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // language change
        $this->middleware('common');
    
        $this->middleware(['admin','organiser'])->except(['organiser_bookings_show', 'delete_booking']);

        $this->event        = new Event;
        $this->ticket       = new Ticket;
        $this->booking      = new Booking;
        $this->transaction  = new Transaction;
        $this->commission   = new Commission;
    }
    
    /**
     * Show my booking
     *
     * @return array
     */
    public function index()
    {
        // get prifex from eventmie config
        $path = false;
        if(!empty(config('eventmie.route.prefix')))
            $path = config('eventmie.route.prefix');

        return Eventmie::view('eventmie::bookings.organiser_bookings', compact('path'));
        
    }

    /**
     * ================== Organiser Booking==============================================================
     */
    
    // get_organiser_bookings
    public function organiser_bookings()
    {
        $params     = [
            'organiser_id'  => Auth::id(),
        ];

        $bookings    = $this->booking->get_organiser_bookings($params);
        
        return response([
            'bookings'  => $bookings->jsonSerialize(),
            'currency'  => setting('regional.currency_default'),
        ], Response::HTTP_OK);
    }

    // booking edit for customer by organiser
    public function organiser_bookings_edit(Request $request)
    {
        $request->validate([
            'event_id'           => 'required|numeric',
            'ticket_id'          => 'required|numeric',
            'booking_id'         => 'required|numeric',
            'customer_id'        => 'required|numeric',
            'booking_cancel'     => 'required|numeric',
            'status'             => 'numeric|nullable',
        ]);

        $params = [
            'event_id'         => $request->event_id,
            'ticket_id'        => $request->ticket_id,
            'id'               => $request->booking_id,
            'organiser_id'     => Auth::id(),
            'customer_id'      => $request->customer_id,
        ];

        // check booking id in booking table for organiser
        $check_booking     = $this->booking->organiser_check_booking($params);

        if(empty($check_booking))
            return error(__('eventmie-pro::em.booking').' '.__('eventmie-pro::em.not_found'), Response::HTTP_BAD_REQUEST );
        
        $start_date              = Carbon::parse($check_booking['event_start_date'].' '.$check_booking['event_start_time']);
        $end_date                = Carbon::parse(Carbon::now());
        
        // check date expired or not
        if($end_date > $start_date)
            return error(__('eventmie-pro::em.cancellation').' '.__('eventmie-pro::em.failed'), Response::HTTP_BAD_REQUEST );

        // pre booking time cancellation check    
        $pre_cancellation_time   = (float) setting('booking.pre_cancellation_time'); 
        $min                     = number_format((float)($start_date->diffInMinutes($end_date) ), 2, '.', '');
        $hour_difference         = (float)sprintf("%d.%02d", floor($min/60), $min%60);
        
        if($pre_cancellation_time > $hour_difference)
            return error(__('eventmie-pro::em.cancellation').' '.__('eventmie-pro::em.failed'), Response::HTTP_BAD_REQUEST );

        $params = [
            'event_id'         => $request->event_id,
            'ticket_id'        => $request->ticket_id,
            'id'               => $request->booking_id,
            'organiser_id'     => Auth::id(),
            'customer_id'      => $request->customer_id,
        ];

        $data = [
            'booking_cancel'   => $request->booking_cancel,
            'status'           => $request->status ? $request->status : 0 ,
        ];
        // booking edit
        $booking_edit    = $this->booking->organiser_edit_booking($data, $params);

        if(empty($booking_edit))
            return error(__('eventmie-pro::em.cancellation').' '.__('eventmie-pro::em.failed'), Response::HTTP_BAD_REQUEST );


        $params = [
            'booking_id'       => $request->booking_id,
            'organiser_id'     => Auth::id(),
            'status'           => $request->status ? $request->status : 0,
        ];
       
        // edit commision table status when change booking table status change by organiser 
        $edit_commission  = $this->commission->edit_commission($params);    

        if(empty($edit_commission))
            return error(__('eventmie-pro::em.commission').' '.__('eventmie-pro::em.not_found'), Response::HTTP_BAD_REQUEST );
        
        // ====================== Notification ====================== 
        //send notification after bookings
        $mail['mail_subject']   = __('eventmie-pro::em.booking').' '.__('eventmie-pro::em.cancellation').' '.__('eventmie-pro::em.successful');
        $mail['mail_message']   = __('eventmie-pro::em.manage').' '.__('eventmie-pro::em.bookings');
        $mail['action_title']   = __('eventmie-pro::em.view').' '.__('eventmie-pro::em.my').' '.__('eventmie-pro::em.bookings');
        $mail['action_url']     = route('eventmie.obookings_index');
        $mail['n_type']       = "cancel";

        
        $notification_ids       = [1, Auth::id()];
        
        $users = User::whereIn('id', $notification_ids)->get();
        \Notification::send($users, new MailNotification($mail));
        // ====================== Notification ======================  
        
        return response([
            'status'=> true,
        ], Response::HTTP_OK);
    }

    // view coustomer booking by oraganiser
    public function organiser_bookings_show($id = null)
    {
        $id    = (int) $id;
        $organiser_id  = Auth::id(); 

        if(!$id)
              // redirect no matter what so that it never turns back
              return response(['status'=>__('eventmie-pro::em.invalid').' '.__('eventmie-pro::em.data'), 'url'=>'/events'], Response::HTTP_OK);    

        // admin can see booking detail page
        if(Auth::user()->hasRole('admin'))
        {
            // when admin wiil be login and he can see booking help or organiser id
            $params   = [
                'id'  => $id,
            ];

            $booking   = $this->booking->organiser_check_booking($params);
            if(empty($booking))
                // redirect no matter what so that it never turns back
                return success_redirect(__('eventmie-pro::em.booking').' '.__('eventmie-pro::em.not_found'), route('eventmie.events_index'));  

            $organiser_id  = $booking->organiser_id;
        }

        $params = [
            'organiser_id' => $organiser_id,
            'id'           => $id,
        ];

        // get customer booking by orgniser
        $booking = $this->booking->organiser_view_booking($params);   
    
        if(empty($booking))
        {
            // redirect no matter what so that it never turns back
            return success_redirect(__('eventmie-pro::em.booking').' '.__('eventmie-pro::em.not_found'), route('eventmie.events_index'));  
        }    

        $currency   = setting('regional.currency_default');
        
        $params = [
            'transaction_id' => $booking['transaction_id'],
            'order_number'   => $booking['order_number']
        ];

        // get transaction information by orgniser for this booking
        $payment = $this->transaction->organiser_payment_info($params);   
        
        return Eventmie::view('eventmie::bookings.show', compact(['booking', 'payment', 'currency']));    

    }

    /**
     *   only admin can delete booking
     */

    public function delete_booking($id = null)
    {
        // only admin can delete booking
        if(Auth::check() && !Auth::user()->hasRole('admin'))
        {
            return redirect()->route('eventmie.events');
        }

        // get event by event_slug
        if(empty($id))
            return error('Booking Not Found!', Response::HTTP_BAD_REQUEST );
        
        $params    = [
            'id'     => $id,
        ];

        $delete_booking     = $this->booking->delete_booking($params);

        if(empty($delete_booking))
        {
            return error('Booking Could Not Deleted!', Response::HTTP_BAD_REQUEST );   
        }

        $msg = __('eventmie-pro::em.booking').' '.__('eventmie-pro::em.deleted').' '.__('eventmie-pro::em.successfully');
        
        return redirect()
        ->route("voyager.bookings.index")
        ->with([
            'message'    => $msg,
            'alert-type' => 'success',
        ]);
        
    }


}
