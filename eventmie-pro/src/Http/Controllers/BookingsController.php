<?php           

namespace Classiebit\Eventmie\Http\Controllers;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Auth;
use Classiebit\Eventmie\Services\PaypalExpress;
use Classiebit\Eventmie\Notifications\MailNotification;
use Classiebit\Eventmie\Notifications\BookingNotification;
use Classiebit\Eventmie\Models\Event;
use Classiebit\Eventmie\Models\Ticket;
use Classiebit\Eventmie\Models\Booking;
use Classiebit\Eventmie\Models\User;
use Classiebit\Eventmie\Models\Commission;
use Classiebit\Eventmie\Models\Transaction;


class BookingsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // language change
        $this->middleware('common');
    
        // this middleware work all functions but not work get_tickets because it is public function
        $this->middleware('auth')->except('get_tickets');

        $this->event        = new Event;
        $this->ticket       = new Ticket;
        $this->booking      = new Booking;
        $this->transaction  = new Transaction;
        $this->user         = new User;
        $this->commission   = new Commission;
        $this->customer_id  = null;
        $this->organiser_id = null;
    }

    // only customers can book tickets so check login user customer or not but admin and organisers can book tickets for customer
    protected function is_admin_organiser(Request $request)
    {
        
        if(Auth::check())
        {
            // get event by event_id
            $event          = $this->event->get_event(null, $request->event_id);
            
            // if event not found then access denied
            if(empty($event))
                return ['status' => false, 'error' =>  __('eventmie-pro::em.event').' '.__('eventmie-pro::em.not_found')];
            
                
            // organiser can't book other organiser event's tikcets but  admin can book any organiser events'tikcets for customer
            if(Auth::user()->hasRole('organiser'))
            {
                if(Auth::id() != $event->user_id)
                    return false;
            }
            
            //organiser_id 
            $this->organiser_id = $event->user_id;
            
            // if login user is customer then 
            // customer id = Auth::id();
            $this->customer_id = Auth::id();

            // if admin and organiser is creating booking
            // then user Auth::id() as $customer_id
            // and customer id will be the id selected from Vue dropdown
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('organiser') )
            {
                // 1. validate data
                $request->validate([
                    'customer_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
                ], [
                    'customer_id.*' => __('eventmie-pro::em.customer').' '.__('eventmie-pro::em.required'),
                ]);
                $this->customer_id = $request->customer_id;
            }

            return true;
        }    
    }

    // get tickets and it is public
    public function get_tickets(Request $request)
    {   
        // 1. validate data
        $request->validate([
            'event_id'       => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
        ]);

        $params    = [
            'event_id' =>  (int) $request->event_id,
        ];

        $tickets   = $this->ticket->get_event_tickets($params);

        if(empty($tickets))
        {
            return response()->json(['status' => false]);    
        }
        
        $customers     = [];
        // get customers when admin and organisers login only becauser admin and organisers can book tickets for customer
        if(Auth::check())
        {
            
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('organiser') )
            {
                $customers  = $this->event->get_customers();
            }
        }    

        return response()->json(['tickets' => $tickets, 'customers' => $customers,  'status' => true, 
                'currency' => setting('regional.currency_default')
        ]);
    }

    

    // check for available seats
    protected function availability_validation($params = [])
    {
        $event_id           = $params['event_id'];
        $selected_tickets   = $params['selected_tickets'];
        $ticket_ids         = $params['ticket_ids'];
        
        // 1. Check booking.max_ticket_qty
        foreach($selected_tickets as $key => $value)
        {
            // user can't book tickets more than limitation 
            if($value['quantity'] > setting('booking.max_ticket_qty')) 
            {
                $msg = __('eventmie-pro::em.max').' '.__('eventmie-pro::em.ticket').' '.__('eventmie-pro::em.quantity').' '.__('eventmie-pro::em.limit').' : ';
                return ['status' => false, 'error' => $msg.setting('booking.max_ticket_qty')];
            }
        }

        // 2. Check availability over booked tickets
        $params   = [
            'event_id'      => $event_id,
            'ticket_ids'    => $ticket_ids,
        ];

        $booked_tickets       = $this->booking->get_booked_tickets($params);
        
        // no bookings yet
        if($booked_tickets->isEmpty())
            return ['status'   => true];

        // false condition
        // selected tickets > 
        // actual tickets (quantity) - already booked tickets (total_quantity)
        foreach($booked_tickets as $key => $val)
        {
            foreach($selected_tickets as $k => $v)
            {
                if($val->ticket_id == $v['ticket_id'])
                {
                    $available = $val->quantity - $val->total_quantity;
                    
                    // false condition
                    // if selected ticket quantity is greator than available
                    if( $v['quantity'] > $available )
                        return ['status' => false, 'error' => __('eventmie-pro::em.booking').' '.__('eventmie-pro::em.failed').' '.$val->title .' '.__('eventmie-pro::em.out_of_stock')];
                }
            }
        }
        
        return ['status'   => true];
    }

    // validate user post data
    protected function general_validation(Request $request)
    {
        
        $request->validate([
            'event_id'          => 'required|numeric|min:1|regex:^[1-9][0-9]*$^',
            
            'ticket_id'         => ['required', 'array'],
            'ticket_id.*'       => ['required', 'numeric'],
            
            'quantity'          => [ 'required', 'array'],
            'quantity.*'        => [ 'required', 'numeric'],

            // repetitve booking date validation
            'booking_date'      => 'date_format:Y-m-d|required',
            'start_time'        => 'date_format:H:i:s|required',
            'end_time'          => 'date_format:H:i:s|required',
        ]);

        if(!empty($request->merge_schedule))
        {
            $request->validate([
                'booking_end_date'      => 'date_format:Y-m-d|required',
            ]);
                
        }
        
        // get event by event_id
        $event          = $this->event->get_event(null, $request->event_id);
        
        // if event not found then access denied
        if(empty($event))
            return ['status' => false, 'error' =>  __('eventmie-pro::em.event').' '.__('eventmie-pro::em.not_found')];
        
        // get only ticket_ids which quantity is >0
        $ticket_ids         = [];
        $selected_tickets   = [];
        
        foreach($request->quantity as $key => $val)
        {
            if($val)
            {
                $ticket_ids[]                               = $request->ticket_id[$key];
                $selected_tickets[$key]['ticket_id']        = $request->ticket_id[$key]; 
                $selected_tickets[$key]['quantity']         = $val; 
                $selected_tickets[$key]['ticket_title']     = $request->ticket_title[$key];  
            }
        }
 
        if(empty($ticket_ids))
            return ['status' => false, 'error' => __('eventmie-pro::em.select_a_ticket')];
            
        $params       =  [
            'event_id'   => $request->event_id,
            'ticket_ids' => $ticket_ids,
        ];

        // check ticket in tickets table that exist or not
        $tickets   = $this->ticket->get_event_tickets($params);

        // if ticket not found then access denied
        if(empty($tickets))
            return ['status' => false, 'error' => __('eventmie-pro::em.tickets').' '.__('eventmie-pro::em.not_found')];

        return [
            'status'            => true,
            'event_id'          => $request->event_id,
            'selected_tickets'  => $selected_tickets,
            'tickets'           => $tickets,
            'ticket_ids'        => $ticket_ids,
            'event'             => $event,
            'booking_date'      => $request->booking_date,
            'start_time'        => $request->start_time,
            'end_time'          => $request->end_time,
        ];

    }

    // pre booking time validation
    protected function time_validation($params = [])
    {
        $booking_date           = $params['booking_date'];
        $start_time             = $params['start_time'];
        $start_time             = $params['end_time'];
        
        // booking date is event start date and it is less then today date then user can't book tickets
        $start_date             = Carbon::parse($booking_date.''.$start_time);  
            
        $today_date             = Carbon::parse(Carbon::now());
 
        // 1.Booking date should not be less than today's date
        if($start_date < $today_date)
            return ['status' => false, 'error' => __('eventmie-pro::em.event').' '.__('eventmie-pro::em.ended')];
        
        // 2. Check prebooking time from settings (in hour)
        $default_prebook_time = (float) setting('booking.pre_booking_time');
        
        $min        = number_format((float)($start_date->diffInMinutes($today_date) ), 2, '.', '');
        
        $hours      = (float)sprintf("%d.%02d", floor($min/60), $min%60);

        if($hours < $default_prebook_time)
            return ['status' => false, 'error' => __('eventmie-pro::em.bookings_over')];

        return ['status' => true];    
    }
    

    // book tickets
    public function book_tickets(Request $request)
    {
        // check login user role
        $status = $this->is_admin_organiser($request);

        // organiser can't book other organiser event's tikcets but  admin can book any organiser events'tikcets for customer
        if(!$status)
        {
            return response([
                'status'    => false,
                'url'       => route('eventmie.events_index'),
                'message'   => __('eventmie-pro::em.organiser_note_5'),
            ], Response::HTTP_OK);
        }

        // 1. General validation and get selected ticket and event id
        $data = $this->general_validation($request);
        if(!$data['status'])
            return error($data['error'], Response::HTTP_BAD_REQUEST);
        
        // 2. Check availability
        $check_availability = $this->availability_validation($data);
        if(!$check_availability['status'])
            return error($check_availability['error'], Response::HTTP_BAD_REQUEST);

        // 3. TIMING & DATE CHECK 
        $pre_time_booking   =  $this->time_validation($data);    
        if(!$pre_time_booking['status'])
            return error($pre_time_booking['error'], Response::HTTP_BAD_REQUEST);

        $selected_tickets   = $data['selected_tickets'];
        $tickets            = $data['tickets'];

        
        $booking_date = $request->booking_date;

        $params  = [
            'customer_id' => $this->customer_id,
        ];
        // get customer information by customer id    
        $customer   = $this->user->get_customer($params);

        if(empty($customer))
            return error($pre_time_booking['error'], Response::HTTP_BAD_REQUEST);     

        $booking        = [];
        $price          = 0;
        $total_price    = 0; 
        foreach($selected_tickets as $key => $value)
        {
            $booking[$key]['customer_id']       = $this->customer_id;
            $booking[$key]['customer_name']     = $customer['name'];
            $booking[$key]['customer_email']    = $customer['email'];
            $booking[$key]['organiser_id']      = $this->organiser_id;
            $booking[$key]['event_id']          = $request->event_id;
            $booking[$key]['ticket_id']         = $value['ticket_id'];
            $booking[$key]['quantity']          = $value['quantity'];
            $booking[$key]['status']            = 1; 
            $booking[$key]['created_at']        = Carbon::now();
            $booking[$key]['updated_at']        = Carbon::now();
            $booking[$key]['event_title']       = $data['event']['title'];
            $booking[$key]['event_category']    = $data['event']['category_name'];
            $booking[$key]['event_start_date']  = $booking_date;
            $booking[$key]['event_end_date']    = $request->merge_schedule ? $request->booking_end_date : $booking_date;
            $booking[$key]['event_start_time']  = $request->start_time;
            $booking[$key]['event_end_time']    = $request->end_time;
            $booking[$key]['event_repetitive']  = $data['event']->repetitive > 0 ? 1   : 0;
            $booking[$key]['ticket_title']      = $value['ticket_title'];
            $booking[$key]['item_sku']          = $data['event']['item_sku'];
            $booking[$key]['currency']          = setting('regional.currency_default');
            
            
            foreach($tickets as $k => $v)
            {
                if($v['id'] == $value['ticket_id'])
                {
                    $price       = $v['price'];
                    break;
                }
            }
            $booking[$key]['price']         = $price * $value['quantity'];
            $booking[$key]['ticket_price']  = $price;

            // call calculate price
            $params   = [
                'ticket_id'         => $value['ticket_id'],
                'quantity'          => $value['quantity'],
            ];
    
            // calculating net price
            $net_price    = $this->calculate_price($params);
    
            $booking[$key]['tax']  = $net_price['tax'];
            $booking[$key]['net_price']  = $net_price['net_price'];

        }

        
        
        // call calculate_commission
        $this->calculate_commission($booking);

        // if net price total == 0 then no paypal process only insert data into booking 
        foreach($booking as $k => $v)
        {
            $total_price  += (float)$v['net_price'];
        }
    
        // IF FREE EVENT THEN ONLY INSERT DATA INTO BOOKING TABLE AND DON'T INSERT DATA INTO TRANSACTION TABLE AND DON'T CALLING PAYPAL API
        if($total_price <= 0 || Auth::user()->hasRole('admin'))
        {
            $data = [
                'order_number' => time().rand(1,988),
                'transaction_id' => 0
            ];
            $flag =  $this->finish_booking($booking, $data);

            // in case of database failure
            if(empty($flag))
            {
                return error('Database failure!', Response::HTTP_REQUEST_TIMEOUT);
            }

            // redirect no matter what so that it never turns backreturn response
            $msg = __('eventmie-pro::em.congrats').' '.__('eventmie-pro::em.booking').' '.__('eventmie-pro::em.successful');
            session()->flash('status', $msg);

            return response([
                'status'    => true,
                'url'       => Auth::user()->hasRole('organiser') ? route('eventmie.obookings_index') : route('eventmie.mybookings_index'),
                'message'   => $msg,
            ], Response::HTTP_OK);
        }    
        
        // return to paypal
        session(['booking'=>$booking]);

        return $this->init_checkout($booking);
    }

     /** 
     * Initialize checkout process
     * 1. Validate data and start checkout process
    */
    protected function init_checkout($booking)
    {   
        // add all info into session
        $order = [
            'item_sku'          => $booking[key($booking)]['item_sku'],
            'order_number'      => time().rand(1,988),
            'product_title'     => $booking[key($booking)]['event_title'],
            
            'price_title'       => '',
            'price_tagline'     => '',
        ];

        $total_price   = 0;

        foreach($booking as $key => $val)
        {
            $order['price_title']   .= ' | '.$val['ticket_title'].' | ';
            $order['price_tagline'] .= ' | '.$val['quantity'].' | ';
            $total_price            += $val['net_price'];
        }
        
        // calculate total price
        $order['price']             = $total_price;

        // set session data
        session(['pre_payment' => $order]);
        
        return $this->paypal($order, setting('regional.currency_default'));
    }

    /* =================== PAYPAL ==================== */
    // 2. Create an order and redirect to payment gateway
    protected function paypal($order = [], $currency = 'USD')
    {
        $paypal_express = new PaypalExpress(setting('apps'));
        $flag           = $paypal_express->create_order($order, $currency);

        // if order creation successful then redirect to paypal
        if($flag['status'])
            return response(['status' => true, 'url'=>$flag['url'], 'message'=>''], Response::HTTP_OK);    

        return error($flag['error'], Response::HTTP_REQUEST_TIMEOUT);
    }
    
    // 3. On return from gateway check if payment fail or success
    public function paypal_callback(Request $request)
    {
        /* Filter out direct fake callback request */
        if(empty(session('paypal_payment_id')))
        {
            $msg = __('eventmie-pro::em.booking').' '.__('eventmie-pro::em.failed');
            // if customer then redirect to mybookings
            $url = route('eventmie.mybookings_index');
            if(Auth::user()->hasRole('organiser'))
                $url = route('eventmie.obookings_index');

            return redirect($url)->withErrors([$msg]);
        }

        $paypal_express = new PaypalExpress(setting('apps'));
        $flag           = $paypal_express->callback($request);

        // IMPORTANT!!! clear session data setted during checkout process
        session()->forget(['paypal_payment_id']);

        return $this->finish_checkout($flag);
    }    

    /* =================== PAYPAL ==================== */

    /** 
     * 4 Finish checkout process
     * Last: Add data to purchases table and finish checkout
    */
    protected function finish_checkout($flag = [])
    {
        // prepare data to insert into table
        $data                   = session('pre_payment');
        // unset extra columns
        unset($data['product_title']);
        unset($data['price_title']);
        unset($data['price_tagline']);
        
        $data['txn_id'] = $flag['transaction_id'];

        $booking                = session('booking');
        
        // IMPORTANT!!! clear session data setted during checkout process
        session()->forget(['pre_payment', 'booking']);
        
        
        // if customer then redirect to mybookings
        $url = route('eventmie.mybookings_index');
        if(Auth::user()->hasRole('organiser'))
            $url = route('eventmie.obookings_index');
        
        if(Auth::user()->hasRole('admin'))
            $url = route('voyager.bookings.index');

        // if success 
        if($flag['status'])
        {
            $data['amount_paid']        = $data['price'];
            unset($data['price']);
            $data['payment_status']     = $flag['message'];
            $data['payer_reference']    = $flag['payer_reference'];
            $data['status']             = 1;
            $data['created_at']         = Carbon::now();
            $data['updated_at']         = Carbon::now();
            $data['currency_code']      = setting('regional.currency_default');
            $data['payment_gateway']    = 'paypal';
            
            // insert data of paypal transaction_id into transaction table
            $flag                       = $this->transaction->add_transaction($data);

            $data['transaction_id']     = $flag; // transaction Id
            
            $flag = $this->finish_booking($booking, $data);

            // in case of database failure
            if(empty($flag))
            {
                
                $msg = __('eventmie-pro::em.booking').' '.__('eventmie-pro::em.failed');
                session()->flash('status', $msg);
                return error_redirect($msg);
            }

            // redirect no matter what so that it never turns back
            $msg = __('eventmie-pro::em.congrats').' '.__('eventmie-pro::em.booking').' '.__('eventmie-pro::em.successful');
            session()->flash('status', $msg);
            return success_redirect($msg, $url);
        }
        
        // if fail
        // redirect no matter what so that it never turns back
        $msg = __('eventmie-pro::em.payment').' '.__('eventmie-pro::em.failed');
        session()->flash('error', $msg);
        
        
        return error_redirect($msg);
    }

    // 5. finish booking
    protected function finish_booking($booking = [], $data = [])
    {
        $admin_commission   = setting('multi-vendor.admin_commission');
            
        $params = [];
        foreach($booking as $key => $value)
        {
            $params[$key] = $value;
            $params[$key]['order_number']    = $data['order_number'];
            $params[$key]['transaction_id']  = $data['transaction_id'];
        }
        
        // get booking_id
        // update commission session array
        // insert into commission
        $commission_data            = [];
        $commission                 = session('commission');

        // delete commission data from session
        session()->forget(['commission']);
        $booking_data = [];
        foreach($booking as $key => $value)
        {
            $data     = $this->booking->make_booking($params[$key]);
            $booking_data[] = $data;

            $commission_data[$key]                 = $commission[$key];
            $commission_data[$key]['booking_id']   = $data->id;
            $booked_date                           = Carbon::parse($data->created_at);
            $commission_data[$key]['month_year']   = $booked_date->format('m Y');
            $commission_data[$key]['created_at']   = Carbon::now();
            $commission_data[$key]['updated_at']   = Carbon::now();
        }
        
        // if have no commission then don't insert data into commission table
        if($admin_commission > 0)
        {
            // insert data in commission table
            $this->commission->add_commission($commission_data);
        }
        
        // ====================== Notification ====================== 
        //send notification after bookings
        $mail['mail_data']      = $booking_data;
        $mail['action_title']   = __('eventmie-pro::em.download').' '.__('eventmie-pro::em.tickets');
        $mail['action_url']     = route('eventmie.mybookings_index');
        $mail['mail_subject']       = __('eventmie-pro::em.congrats').' '.__('eventmie-pro::em.booking').' '.__('eventmie-pro::em.successful');
        $mail['n_type']         = "bookings";

        $notification_ids       = [1, $booking[key($booking)]['organiser_id'], $booking[key($booking)]['customer_id']];
        
        $users = User::whereIn('id', $notification_ids)->get();
        \Notification::locale(\App::getLocale())->send($users, new BookingNotification($mail));
        // ====================== Notification ====================== 
        
                
        return true;
    }

    /**
     *  calculate net price for paypal
     */

    protected function calculate_price($params = [])
    {
        // check ticket in tickets table that exist or not
        $ticket   = $this->ticket->get_ticket($params);

        
        $net_price      = [];
        $amount         = 0;
        $tax            = 0;
        
        $amount  = $ticket['price']*$params['quantity'];

        $net_price['tax']         = $tax;
        $net_price['net_price']   = $tax+$amount;
        
        //if have no taxes then return net_price
        if(empty($ticket['rate_type']))
            return $net_price;
            
        // in case of percentage
        if($ticket['rate_type'] == 'percent')
        {
            $tax     = (float)($ticket['price'] * $params['quantity'] * $ticket['rate'])/100; 
            // in case of including
            if($ticket['net_price'] == 'including')
            {
                $net_price['tax']        = -$tax;
                $net_price['net_price']  = $amount;
            }

            // in case of excluding
            if($ticket['net_price'] == 'excluding')
            {
                $net_price['tax']        = $tax;
                $net_price['net_price']   = $tax+$amount;
            }
        }
        
        // // in case of percentage
        if($ticket['rate_type'] == 'fixed')
        {
            $tax                     = (float)$params['quantity'] * $ticket['rate'];
            
            // // in case of including
            if($ticket['net_price'] == 'including')
            {   
                $net_price['tax']         = -$tax;
                $net_price['net_price']   = $amount; 
            }    
            
            // // in case of excluding
            if($ticket['net_price'] == 'excluding')
            {   
                $net_price['tax']         = $tax;
                $net_price['net_price']   = $amount + $tax;
            }   

        }
        return $net_price;
    }

    // calculate admin commission
    protected function calculate_commission($booking = [])
    {
        $commission         = [];
        $admin_commission   = setting('multi-vendor.admin_commission');
           
        foreach($booking as $key => $value)
        {
            $commission[$key]['organiser_id']         = $value['organiser_id'];
            $commission[$key]['customer_paid']        = $value['net_price'];
            $commission[$key]['admin_commission']     = $admin_commission;

            $margin = (float) ( ($admin_commission * $value['net_price']) /100 );

            $commission[$key]['organiser_earning']    = (float) $value['net_price'] - $margin;
        }
    
        session(['commission'=>$commission]);
    }
    
}
