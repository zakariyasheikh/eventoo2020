<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Commission extends Model
{
    protected $guarded = [];

    // add commission in commission table
    public function add_commission($params = [])
    {
        return Commission::insert($params);
    }

    // edit commision table status when change booking table status change by organiser
    public function edit_commission($params)
    {
        $commission = Commission::
                        where([
                            'organiser_id'  => $params['organiser_id'], 
                            'booking_id'    => $params['booking_id'], 
                        ])->first();
        
        // commission is optional so some bookings have no commission so no update then return true                
        // if have no commission in commision in commission tabel, will return true because commission is optional 
        if(empty($commission))
            return true;

        return Commission::
        where([
            'organiser_id'  => $params['organiser_id'], 
            'booking_id'    => $params['booking_id'], 
        ])
        ->update(['status' => $params['status']]);
    }

    // show all commisssion of organisers for admin
    public function admin_commission()
    {
        $mode           = config('database.connections.mysql.strict');

        $table          = 'commissions'; 
        $query          = DB::table($table);
        

        if(!$mode)
        {
            // safe mode is off
            $select = array(
                           
                            "$table.organiser_id as org_id",
                            DB::raw("(SELECT U.name FROM users U WHERE U.id = org_id) as organiser_name"),
                            DB::raw("SUM($table.customer_paid) as customer_paid_total"),
                            DB::raw("SUM($table.admin_commission) as admin_commission_total"),
                            DB::raw("SUM($table.organiser_earning) as organiser_earning_total"),
                            "$table.updated_at",
                        );
        }
        else
        {
            // safe mode is on
            $select = array(
                            DB::raw("ANY_VALUE($table.organiser_id) as org_id"),
                            DB::raw("(SELECT U.name FROM users U WHERE U.id = org_id) as organiser_name"),
                            DB::raw("SUM($table.customer_paid) as customer_paid_total"),
                            DB::raw("SUM($table.admin_commission) as admin_commission_total"),
                            DB::raw("SUM($table.organiser_earning) as organiser_earning_total"),
                            DB::raw("ANY_VALUE($table.updated_at) as updated_at"),
                        );
        }
        
        return   $query->select($select)
                            ->groupBy("org_id")
                            ->get()
                            ->toArray();
    }

    // show  commission organisers and month_year wise for admin
    public function show_commission_organisers_wise($organiser_id = null)
    {
        $mode           = config('database.connections.mysql.strict');

        $table          = 'commissions'; 
        $query          = DB::table($table);
        
        

        if(!$mode)
        {
            // safe mode is off
            $select = array(
                           
                            "$table.organiser_id as org_id",
                            "$table.transferred",
                            "$table.month_year",
                            DB::raw("(SELECT U.name FROM users U WHERE U.id = org_id) as organiser_name"),
                            DB::raw("SUM($table.customer_paid) as customer_paid_total"),
                            DB::raw("SUM($table.admin_commission) as admin_commission_total"),
                            DB::raw("SUM($table.organiser_earning) as organiser_earning_total"),
                            "$table.updated_at",
                        );
        }
        else
        {
            
            // safe mode is on
            $select = array(
                            DB::raw("ANY_VALUE($table.organiser_id) as org_id"),
                            DB::raw("ANY_VALUE($table.transferred) as transferred"),
                            DB::raw("ANY_VALUE($table.month_year) as month_year"),
                            DB::raw("(SELECT U.name FROM users U WHERE U.id = org_id) as organiser_name"),
                            DB::raw("SUM($table.customer_paid) as customer_paid_total"),
                            DB::raw("SUM($table.admin_commission) as admin_commission_total"),
                            DB::raw("SUM($table.organiser_earning) as organiser_earning_total"),
                            DB::raw("ANY_VALUE($table.updated_at) as updated_at"),
                        );
        }
        
        return   $query->select($select)
                            ->where("organiser_id", $organiser_id)
                            ->groupBy("$table.month_year")
                            ->get()
                            ->toArray();
    }

    public function admin_edit_commission($params)
    {
        
        return Commission::
        where([
            'organiser_id'  => $params['organiser_id'], 
            'month_year'    => $params['month_year'], 
            
        ])
        ->update(['transferred' => $params['transferred']]);
    }
}
