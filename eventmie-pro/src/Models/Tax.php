<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $guarded = [];
    
    public function get_taxes()
    {
        return TAX::all()->toArray();
    }
}
