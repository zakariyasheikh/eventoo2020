<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

use Classiebit\Eventmie\Models\User;
use Classiebit\Eventmie\Models\Transaction;

class Booking extends Model
{
    protected $guarded = [];

    /**
     * Table used
    */
    private $tb                 = 'bookings';
    private $tb_tickets         = 'tickets'; 
    
    // make booking
    public function make_booking($params = [])
    {
        return Booking::create($params);
    }    

    // get event's booked tickets
    public function get_booked_tickets($params = [])
    {
        return DB::table($this->tb)
                ->leftJoin($this->tb_tickets, "$this->tb_tickets.id", '=', "$this->tb.ticket_id")
                ->select([
                    "$this->tb.ticket_id",
                    
                    "$this->tb_tickets.title",
                    "$this->tb_tickets.quantity",
                ])
                ->selectRaw("SUM($this->tb.quantity) as total_quantity")
                
                ->whereIn("$this->tb.ticket_id", $params['ticket_ids'])
                ->where("$this->tb.event_id", $params['event_id'])
                ->where("$this->tb.status", 1)

                ->groupBy("$this->tb.ticket_id")
                ->get();
    }

    // get booking for customer
    public function get_my_bookings($params = [])
    {
        return Booking::select('bookings.*')
                ->from('bookings')
                ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
                ->where(['customer_id' => $params['customer_id'] ])
                ->orderBy('id', 'desc')
                ->paginate(10);
    }
    

    // check booking id for cancellation
    public function check_booking($params = [])
    {
        return Booking::
            where([
                'status'        => 1, 
                'customer_id'   => $params['customer_id'], 
                'id'            => $params['booking_id'], 
                'ticket_id'     => $params['ticket_id'], 
                'event_id'      => $params['event_id'] ])
            ->first();   
    }

    // booking_cancel for customer
    public function booking_cancel($params = [])
    {
        return Booking::
                where([
                    'status'        => 1, 
                    'customer_id'   => $params['customer_id'], 
                    'id'            => $params['booking_id'], 
                    'ticket_id'     => $params['ticket_id'], 
                    'event_id'      => $params['event_id'] ])
                ->update(['booking_cancel' => 1 ]);
    }

    /**
     * ================Organiser Booking Start=========================================
     */

     // get booking for organiser
    public function get_organiser_bookings($params = [])
    {
        return Booking::select('bookings.*', 'CM.customer_paid')
                        ->from('bookings')
                        ->selectRaw("(SELECT E.slug FROM events E WHERE E.id = bookings.event_id) event_slug")
                        ->leftJoin('commissions as CM', 'CM.booking_id', '=', 'bookings.id')
                        ->where([ 'bookings.organiser_id' => $params['organiser_id'] ])
                        ->orderBy('id', 'desc')
                        ->paginate(10);
    }
    
    // check booking id for cancellation for organiser
    public function organiser_check_booking($params = [])
    {
        return Booking::
            where($params)
            ->first();   
    }

    // booking_edit for customer by organiser
    public function organiser_edit_booking($data = [], $params = [])
    {
        return Booking::
                where($params)
                ->update($data);
    }

    // organiser view booking of customer
    public function organiser_view_booking($params = [])
    {
        return Booking::select('bookings.*')->from('bookings')
            ->where($params)
            ->first();  
    }

    // only admin can delete booking
    public function delete_booking($params = [])
    {
        return Booking::where($params)
            ->delete();
    }

    // only admin and organiser can get particular event's booking
    public function get_event_bookings($params = [])
    {
        return Booking::where($params)
            ->get()->toArray();
    }

}
