<?php

namespace Classiebit\Eventmie\Models;

use Auth;
use DB;

use Illuminate\Database\Eloquent\Model;

class Speaker extends Model
{
    protected $guarded = [];

    // get all speaker for particular organiser 
    public function get_speakers($params = [])
    {
        return  Speaker::where('user_id', $params['user_id'])->orderBy('updated_at', 'DESC')->paginate(100);
    }

    // get particular event speaker
    public function get_event_speakers($event_id = null)
    {
        
        $speakers_id_temp    = DB::table('event_speaker')->select('speaker_id')->where('event_id', $event_id)->get()->toArray();

        $speakers_id         = [];
        
        if(empty($speakers_id_temp))
        {
            return  false;
            
        }
        
        foreach($speakers_id_temp as $key => $value)
        {
            $speakers_id[$key]  = $value->speaker_id;
        }
        
        return  Speaker::whereIn('id',$speakers_id)->orderBy('updated_at', 'DESC')->get()->toArray();
    }

    // add speaker
    public function add_speakers($params = [], $speaker_id = null)
    {
        if(empty($speaker_id))
        {
            return Speaker::create($params);
        }
        return Speaker::where('id', $speaker_id)->update($params);
    }

    // delete speakers
    public function delete_speakers($speaker_id = null)
    {   
        DB::table('event_speaker')->where('speaker_id', $speaker_id)->delete();
        return Speaker::where(['id' => $speaker_id])->delete();
    }

    // total speaker
    public function total_speakers()
    {
        return Speaker::where(['status' => 1])->count();
    }

    //get selected speaker from event_speaker table when organiser editing his event
    public function selected_event_speakers($event_id = null)
    {
        return DB::table('event_speaker')
                ->select('event_speaker.speaker_id')
                ->selectRaw("(SELECT SP.fullname FROM speakers SP WHERE SP.id = event_speaker.speaker_id) speaker_name")
                ->where(['event_id' =>$event_id])
                ->get()->toArray();  
    }

    // get only one speaker base on speaker id
    public function get_speaker($speaker_name = null)
    {
        return Speaker::whereRaw('LOWER(`fullname`) = "'.$speaker_name.'" ')->first();
    }

}
