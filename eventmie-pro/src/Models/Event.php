<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Event extends Model
{
    
    protected $guarded = [];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     *  total events
     */

    public function total_events()
    {
        return Event::where(['status' => 1])->count();
    }
    
    /* Get event tickets */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    // get event
    public function get_event($slug = null, $event_id = null)
    {   
        return Event::select('events.*')->from('events')
            ->where(['slug' => $slug])
            ->orWhere(['id' => $event_id])    
            ->selectRaw("(SELECT CT.name FROM categories CT WHERE CT.id = events.category_id) category_name")
            ->selectRaw("(SELECT SD.repetitive_type FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
            ->first();
    }

    // check event id that event id have login user or not    
    public function get_user_event($event_id = null, $user_id = null)
    {
        return Event::select('events.*')->from('events')
                    ->where(['id' => $event_id, 'user_id' => $user_id ])
                    ->selectRaw("(SELECT SD.repetitive_type FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
                    ->selectRaw("(SELECT COUNT(BK.id) FROM bookings BK WHERE BK.event_id = events.id  ) count_bookings")
                    ->first();
    }

    // create user event
    public function save_event($params = [], $event_id = null)
    {
       // if have no event id then create new event
        if(empty($event_id))
        {
            return Event::create($params);
        }
        // if have event id then update     
        return Event::where(['id' => $event_id])->update($params);
    }

    
    // add sponsores and speakers 
    public function sponsors_speakers($params = []) 
    {
        $event_speaker = null;
        $event_sponsor = null;

        // delete speakers and sponsors from bridge table then futher process 
        DB::table('event_speaker')->where('event_id', $params['event_id'])->delete();
        DB::table('event_sponsor')->where('event_id', $params['event_id'])->delete();

        // for speakers
        foreach($params['event_speakers'] as $key => $value)
        {
            // insert data into event_speaker table        
            $event_speaker    = DB::table('event_speaker')->insert($value);
        }    
        
        // check event_sponsors that empty or not then insert data into event_speaker table because it optional field 
        if(!empty($params['event_sponsors']))
        {
            foreach($params['event_sponsors'] as $key => $value)
            {
                // insert data into event_sponsor table        
                $event_sponsor   = DB::table('event_sponsor')->insert($value);
            }    
        }
        
        $sponsor_speaker = [
            'event_speaker' => $event_speaker ,
            'event_sponsor' => $event_sponsor ,
        ];

        return $sponsor_speaker;
    }

    

    /**
     * Get events with 
     * pagination and custom selection
     * 
     * @return string
     */
    public function events($params  = [])
    {   
        $query = DB::table('events'); 

            $query
            ->select("events.*");
            if(!empty($params['search']))    
            {
                $query
                ->whereRaw("( title LIKE '%".$params['search']."%' 
                    OR venue LIKE '%".$params['search']."%' OR state LIKE '%".$params['search']."%' OR city LIKE '%".$params['search']."%')");
            }
               
            $query->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
                    ->selectRaw("(SELECT  CT.name FROM categories CT WHERE CT.id = events.category_id) category_name")
                    ->selectRaw("(SELECT SD.repetitive_type  FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type");
           
            if(!empty($params['category_id']))
                $query ->where('category_id',$params['category_id']);
    
            if(!empty($params['start_date']) && !empty($params['end_date']))
            {
                $query ->where('start_date', '>=' , $params['start_date']);
                $query ->where('start_date', '<=' , $params['end_date']);
            }
                
            
            if(!empty($params['start_date']) && empty($params['end_date']))
                $query ->where('start_date', $params['start_date']);
            
            
            if(!empty($params['price']))
            {
                if($params['price'] == 'free')
                    $query ->where('price_type', "0" );
                
                if($params['price'] == 'paid')
                    $query ->where('price_type', 1);    
            }

        $query->where(["events.status" => 1, "events.publish" => 1]);

        // if hide expired events is on
        if(!empty(setting('booking.hide_expire_events')))
        {
            $today  = \Carbon\carbon::now(setting('regional.timezone_default'))->format('Y-m-d');    
            $query->whereRaw('(IF(events.repetitive = 1, events.end_date >= "'.$today.'", events.start_date >= "'.$today.'"))');
        }

        return $query->orderBy('events.start_date', 'ASC')
                    ->paginate(9);
    }
    
    // update price_type column of event table by 1 if have no free tickets
    public function update_price_type($event_id = null, $params = [])
    {
        return Event::where('id', $event_id)->update($params);
    }

    // get featured event for welocme page
    public function get_featured_events()
    {
        return Event::select('events.*')->from('events')
            ->where(['featured' => 1, 'publish' => 1, 'status' => 1])
            ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
            ->selectRaw("(SELECT CT.name FROM categories CT WHERE CT.id = events.category_id) category_name")
            ->selectRaw("(SELECT SD.repetitive_type  FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
            ->limit(6)
            ->get();
    }
    
    // get top selling event
    public function get_top_selling_events()
    {
        return Event::select('events.*')->from('events')
            ->selectRaw("(SELECT SUM(BK.quantity) FROM bookings BK WHERE BK.event_id = events.id) total_booking")
            ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
            ->selectRaw("(SELECT CT.name FROM categories CT WHERE CT.id = events.category_id) category_name")
            ->selectRaw("(SELECT SD.repetitive_type  FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
            ->where(['publish' => 1, 'status' => 1])
            ->orderBy('total_booking', 'desc')
            ->limit(6)
            ->get();
    }
    
    // get upcomming events
    public function get_upcomming_events()
    {
        return  Event::select('events.*')
                    ->whereDate('start_date', '!=', Carbon::now()->format('Y-m-d'))
                    ->whereDate('start_date', '>', Carbon::now()->format('Y-m-d'))
                    ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
                    ->selectRaw("(SELECT CT.name FROM categories CT WHERE CT.id = events.category_id) category_name")
                    ->selectRaw("(SELECT SD.repetitive_type  FROM schedules SD WHERE SD.event_id = events.id limit 1 ) repetitive_type")
                    ->where(['publish' => 1, 'status' => 1])
                    ->orderBy('start_date')
                    ->limit(6)
                    ->get();

    }

    // get organisers 
    public function get_organisers($params = [])
    {
        return  DB::table('users')
                    ->select('name', 'id')
                    ->where('role_id', 3)
                    ->get()
                    ->toArray();
    }

    // get customers
    public function get_customers($params = [])
    {
        return  DB::table('users')
                    ->select('name', 'id')
                    ->where('role_id', 2)
                    ->get()
                    ->toArray();
    }

    

    /**
     * =====================GEt events for particular organiser start=====================================
     */

    // get my evenst of particular organiser 
    public function get_my_events($params = [])
    {
        return Event::select('events.*')
            ->from('events')
            ->selectRaw("(SELECT CN.country_name FROM countries CN WHERE CN.id = events.country_id) country_name")
            ->selectRaw("(SELECT CT.name FROM categories CT WHERE CT.id = events.category_id) category_name")
            ->where(['user_id' => $params['organiser_id'] ])
            ->paginate(9);
    }

    // organiser can disable own event
    public function disable_event($params = [], $data = [])
    {
        return Event::where(['id' => $params['event_id'], 'user_id' => $params['organiser_id']])
            ->update($data);
    }

    // only admin can delete event  
    public function delete_event($params = [])
    {
        return Event::where(['id' => $params['event_id']])
            ->delete();
    }

     /**
      * ============================= End particular organiser events ===================================================
      */
}
