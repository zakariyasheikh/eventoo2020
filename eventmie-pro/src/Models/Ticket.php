<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;
use Composer\DependencyResolver\Request;

class Ticket extends Model
{
    // include
    protected $guarded = [];

   // create and update tickets
    public function add_tickets($params = [], $ticket_id = null)
    {
        if(empty($ticket_id))
        {
            // create tickets
            return Ticket::create($params);
        }
        // update ticket
        return Ticket::where('id', $ticket_id)->update($params);
    }

    /**
     * Get only one event's tickets  when param is emtpy 
      */
    public function get_event_tickets($params = [])
    {
        if(!empty($params['ticket_ids']))
            return Ticket::select('tickets.*', 'TX.title as tax_title', 'TX.rate_type', 'TX.rate', 'TX.net_price', 'TX.id as tax_id')
                    ->from('tickets')
                    ->leftJoin('taxes as TX', 'tickets.tax_id', '=', 'TX.id')
                    ->whereIn('tickets.id', $params['ticket_ids'])
                    ->where('tickets.event_id', $params['event_id'])
                    ->orderBy('price')
                    ->get()->toArray();

        return Ticket::select('tickets.*', 'TX.title as tax_title', 'TX.rate_type', 'TX.rate', 'TX.net_price', 'TX.id as tax_id')
                    ->from('tickets')
                    ->leftJoin('taxes as TX', 'tickets.tax_id', '=', 'TX.id')
                    ->where(['event_id' => $params['event_id'] ])
                    ->orderBy('price')
                    ->get()->toArray();
        
    }

    /**
     * Get only one ticket 
      */
      public function get_ticket($params = [])
      {
            return Ticket::select('tickets.*', 'TX.title as tax_title', 'TX.rate_type', 'TX.rate', 'TX.net_price', 'TX.id as tax_id')
                    ->from('tickets')
                    ->leftJoin('taxes as TX', 'tickets.tax_id', '=', 'TX.id')
                    ->where('tickets.id', $params['ticket_id'])
                    ->first();
      }

    // delete tickets
    public function delete_tickets($ticket_id = null)
    {   
        return Ticket::where(['id' => $ticket_id])->delete();
    }

    // get tickets for multiple events with related event_id for events listing with two tickets
    public function get_events_tickets($event_ids = [])
    {
        return Ticket::whereIn('event_id',$event_ids)->orderBy('price')->get()->toArray();
    }

    // check free tickets with related event id
    public function check_free_tickets($event_id = null)
    {
        return Ticket::where('price',"0")->where('event_id', $event_id)->get()->toArray();
    }

}
