<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class Sponsor extends Model
{
    // it will only take these fields from the post request
    
    // include
    protected $guarded = [];

    
    // add sopnors 
    public function add_sponsors($params = [], $sponsor_id = null)
    {
        if(empty($sponsor_id))
        {
            return Sponsor::insertGetId($params);
        }
        return Sponsor::where('id', $sponsor_id)->update($params);
    }

    // get all sponsors for login organiser and particular event
    public function get_sponsors($params =[])
    {
        return  Sponsor::where('user_id',$params['user_id'])->orderBy('updated_at', 'DESC')->paginate(100);
    }

    // get particular event sponsors for event listing page means get sponsor for one event
    public function get_event_sponsors($event_id = null)
    {
        $sponsors_id_temp    = DB::table('event_sponsor')->select('sponsor_id')->where('event_id', $event_id)->get()->toArray();

        $sponsors_id         = [];
        
        if(empty($sponsors_id_temp))
        {
            return false; 
        }
        
        foreach($sponsors_id_temp as $key => $value)
        {
            $sponsors_id[$key]  = $value->sponsor_id;
        }
        
        return Sponsor::whereIn('id', $sponsors_id)->orderBy('updated_at', 'DESC')->get()->toArray();
    }

    // delete sponsors
    public function delete_sponsors($sponsor_id = null)
    {
        DB::table('event_sponsor')->where('sponsor_id', $sponsor_id)->delete();
        return Sponsor::where(['id' => $sponsor_id])->delete();
    }

    // total sponsors count
    public function total_sponsors()
    {
        return Sponsor::where(['status' => 1])->count();
    }

    //get selected sponsors from event_sponsors table when organiser editing his event
    public function selected_event_sponsors($event_id = null)
    {
        return DB::table('event_sponsor')
                ->select('event_sponsor.sponsor_id')
                ->where(['event_id' =>$event_id])
                ->selectRaw("(SELECT SP.name FROM sponsors SP WHERE SP.id = event_sponsor.sponsor_id) sponsor_name")
                ->get()
                ->toArray();  
    }

    // get sponsor by id only one sponsor
    public function get_sponsor($sponsor_id = null)
    {
        return Sponsor::where(['id' => $sponsor_id])->first();
    }

}
