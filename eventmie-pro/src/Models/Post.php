<?php

namespace Classiebit\Eventmie\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends \TCG\Voyager\Models\Post
{
    protected $guarded = [];
    // posts with limit for welcome page
    public function index()
    {
        return Post::where(['status' => 'PUBLISHED'])
                ->limit(3)->get()->toArray();

    }

    // particular post view
    public function view($slug = null)
    {
        return Post::where(['slug' => $slug])->first();
        
    }

    // get posts
    public function get_posts()
    {
        return Post::where(['status' => 'PUBLISHED'])->paginate(9);
    }

}    