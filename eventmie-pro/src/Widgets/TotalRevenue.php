<?php

namespace Classiebit\Eventmie\Widgets;

use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use Facades\Classiebit\Eventmie\Eventmie;
use TCG\Voyager\Widgets\BaseDimmer;

class TotalRevenue extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = \Classiebit\Eventmie\Models\Booking::sum('net_price');
        


        // $count  = Voyager::model('Page')->count();
        $string = trans_choice('Revenue', $count);

        return Eventmie::view('eventmie::widgets.total_revenue', array_merge($this->config, [
            'icon'   => 'voyager-dollar',
            'title'  => "{$count} ".setting('regional.currency_default')." {$string}",
            'text'   => __('Total Revenue', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('view all Revenue'),
                'link' => route('eventmie.events_index'),
            ],
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
