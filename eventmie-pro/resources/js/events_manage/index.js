
/**
 * This is a page specific seperate vue instance initializer
 */

// include vue common libraries, plugins and components
require('../vue_common');

/**
 * Below are the page specific plugins and components
  */

// for using time
window.moment   = require('moment-timezone');



// add Veevalidate for auto validation
window.VeeValidate = require('vee-validate');
Vue.use(VeeValidate)

// add Vuex for global variables management across components
window.Vuex = require('vuex');
Vue.use(Vuex);

Vue.component('tabs-component', require('./components/Tabs.vue').default);

// import Vuerouter
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// import component for vue routes
import Detail from './components/Detail';
import Media from './components/Media';
import Location from './components/Location';
import Timing from './components/Timing';
import Tickets from './components/Tickets';
import Poweredby from './components/Poweredby';
import Seo from './components/Seo';

// vue routes
const routes = new VueRouter({
    // mode: 'history',
    // base: '/profile',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'detail',
            component: Detail,
            props: true,
        },
        {
            path: '/media',
            name: 'media',
            component: Media,
            props: true,
        },
        {
            path: '/seo',
            name: 'seo',
            component: Seo,
            props: true,
        },
        {
            path: '/location',
            name: 'location',
            component: Location,
            props: true,
        },
        {
            path: '/timing',
            name: 'timing',
            component: Timing,
            props: true,
        },
        {
            path: '/tickets',
            name: 'tickets',
            component: Tickets,
            props: true,
        },
        {
            path: '/poweredby',
            name: 'poweredby',
            component: Poweredby,
            props: true,
        },
    ],
});


// declare a global store object
const store = new Vuex.Store({
    state: {
        event        : [],
        sponsors     : [],
        tickets      : [],
        speakers     : [],
        event_id     : null,
        
        v_sch_index         : 0,
        v_repetitive        : 0,
        v_repetitive_days   : [],
        v_repetitive_dates  : [],
        v_from_time         : [],
        v_to_time           : [],
        organiser_id        : null,

    },
    mutations: {
        add(state, {sponsors, tickets, speakers, event_id, v_repetitive, v_repetitive_days, v_repetitive_dates, v_from_time, v_to_time, organiser_id, event}) {
            if(typeof sponsors !== "undefined") {
                state.sponsors     = sponsors;
            }

            if(typeof tickets !== "undefined") {
                state.tickets   = tickets;
            }

            if(typeof speakers !== "undefined") {
                state.speakers   = speakers;
            }

            if(typeof event_id !== "undefined") {
                state.event_id   = event_id;
            }

            if(typeof v_repetitive !== "undefined") {
                state.v_repetitive   = v_repetitive;
            }

            if(typeof v_repetitive_days !== "undefined") {
                state.v_repetitive_days = v_repetitive_days;
            }

            if(typeof v_repetitive_dates !== "undefined") {
                state.v_repetitive_dates = v_repetitive_dates;
            }

            if(typeof v_from_time !== "undefined") {
                state.v_from_time = v_from_time;
            }

            if(typeof v_to_time !== "undefined") {
                state.v_to_time = v_to_time;
            }

            if(typeof organiser_id !== "undefined") {
                state.organiser_id = organiser_id;
            }

            if(typeof event !== "undefined") {
                state.event = event;
            }

            

        },
        update(state,{sponsors, v_sch_index, v_repetitive_days, v_repetitive_dates, v_from_time, v_to_time}){

            if(typeof sponsors !== "undefined") {
                // in case of multiple items
                if(sponsors.length > 1) 
                    state.sponsors.push(...sponsors);
                else
                    state.sponsors.push(sponsors);
            }

            if(typeof v_repetitive_days !== "undefined" && typeof v_sch_index !== "undefined"  ) {
                state.v_repetitive_days[v_sch_index] = v_repetitive_days;
            }

            if(typeof v_repetitive_dates !== "undefined" && typeof v_sch_index !== "undefined"  ) {
                state.v_repetitive_dates[v_sch_index] = v_repetitive_dates;
            }

            if(typeof v_from_time !== "undefined" && typeof v_sch_index !== "undefined"  ) {
                state.v_from_time[v_sch_index] = v_from_time;
            }

            if(typeof v_to_time !== "undefined" && typeof v_sch_index !== "undefined"  ) {
                state.v_to_time[v_sch_index] = v_to_time;
            }
        },
    },
});

/**
 * This is where we finally create a page specific
 * vue instance with required configs
 * element=app will remain common for all vue instances
 * 
 */
window.app = new Vue({
    el: '#eventmie_app',
    store: store,
    router: routes

});