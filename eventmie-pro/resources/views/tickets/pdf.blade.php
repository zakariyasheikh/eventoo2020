<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<style>
    * {
        font-family: sans-serif;
    }
    body {
        margin: 0 auto !important;
        padding: 0 !important;
        height: 100% !important;
        width: 100% !important;
        font-size: 12px;
        font-family: sans-serif;
    }
    h1, h2, h3 ,h4 {
        color: #3c3c3c;
        margin: 5px 0;
    }
    h1 {
        font-size: 14px;
    }
    h2 {
        font-size: 12px;
    }
    ul {
        list-style-type: none;
    }
    table {
        width: 95%;
        padding: 5px;
        margin: 0 auto !important;
        border-spacing: 0 !important;
        border-collapse: collapse !important;
        table-layout: fixed !important;
    }
    table table table {
        table-layout: auto;
    }
    table td {
        padding: 5px;
        font-size: 12px;
    }
    a {
        text-decoration: none;
    }

    .center {
        text-align: center;
    }

    .text-left {
        text-align: left;
    }

    .text-right {
        text-align: right;
    }
    .pull-left {
        float: left;
    }

    .pull-right {
        float: right;
    }

    .s-heading {
        font-weight: 600;
        font-size: 12px;
    }
    .m-heading {
        font-weight: 400;
        font-size: 14px;
    }
    .b-heading {
        font-weight: 600;
        font-size: 14px;
    }
    .p-0 {
        padding: 0 !important;
    }
    .pt-10 {
        padding-top: 10px !important;
    }
    .pt-20 {
        padding-top: 20px !important;
    }
    .pb-0 {
        padding-bottom: 0 !important;
    }
    .t-price {
        color: #00adef;
        font-weight: 600;
    }
    .small-text {
        font-size: 10px;
        color: #797979;
    }
    .tiny-text {
        font-size: 8px;
        color: #797979;
    }
    .title-bar {
        background: #2b2b2b;
        padding: 0 !important;
    }
    .title-bar .s-heading {
        color: #fff;
        font-size: 11px;
        text-transform: uppercase;
    }
    
</style>
</head>
<body>
    <!-- when testing  -->
    {{-- <div style="max-width: 680px;margin: 0 auto;"> --}}
    <!-- when generating  -->
    <div>

        <!-- 1. Event details -->
        <div>
            <table>
                <tr>
                    <td style="padding: 0;width: 35%;">
                        <img style="width: 95%;" src="{{public_path('storage/'.$event->thumbnail)}}">
                    </td>
                    <td>
                        <div>
                            <table>
                                <tr>
                                    <td style="padding: 0;">
                                        <img src="{{public_path('storage/'.setting('site.logo'))}}" style="width: 64px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td><small class="b-heading">{{$event->title}}</small></td>
                                </tr>
                                <tr>
                                    <td><small class="s-heading"> @lang('eventmie-pro::em.venue') </small></td>
                                </tr>
                                <tr>
                                    <td>{{ucfirst($event->venue)}} | {{ucfirst($event->address)}}</td>
                                </tr>
                                <tr>
                                    <td><small class="s-heading">@lang('eventmie-pro::em.timings')</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        {{ date('d-M-Y', strtotime($event->start_date)).' - '.date('d-M-Y', strtotime($event->end_date)) }}
                                        {{ ' ('.date('g:iA', strtotime($event->start_time)).' - '.date('g:iA', strtotime($event->end_time)).')' }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    
                </tr>
            </table>
        </div>
        
        <!-- 2. Booking details -->
        <div>
            <table>
                <tr>
                    <td class="title-bar">
                        <table>
                            <tr>
                                <td class="center" style="width: 37%;"><small class="s-heading">@lang('eventmie-pro::em.ticket')</small></td>
                                <td class="text-left"><small class="s-heading">{{ $booking['ticket_title'] }}</small></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <td class="center" style="padding: 0;width: 40%;">
                        @php $qrcode = $booking['customer_id'].'/'.$booking['id'].'-'.$booking['order_number'].'.png'; @endphp
                        <img src="{{public_path('storage/qrcodes/'.$qrcode)}}" style="width: 70%;">
                    </td>
                    <td>
                        <div>
                            <table>
                                <tr>
                                    <td class="pb-0"><small class="s-heading">@lang('eventmie-pro::em.price') X @lang('eventmie-pro::em.quantity')</small></td>
                                    <td class="pb-0"><small class="s-heading">@lang('eventmie-pro::em.ticket') #</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="t-price">{{$booking['ticket_price']}} {{$currency}}</span> X {{$booking['quantity']}}
                                    </td>
                                    <td>{{ $booking['order_number'] }}</td>
                                </tr>
                                <tr>
                                    <td class="pb-0"><small class="s-heading">@lang('eventmie-pro::em.identity')</small></td>
                                    <td class="pb-0"><small class="s-heading">@lang('eventmie-pro::em.booking') @lang('eventmie-pro::em.date')</small></td>
                                </tr>
                                <tr>
                                    <td>{{ucfirst($booking['customer_name'])}}</td>
                                    <td>{{ date('d M Y g:iA', strtotime($booking['created_at'])) }}</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div>
            <table>
                <tr>
                    <td class="title-bar">
                        <table>
                            <tr>
                                <td class="center" style="width: 37%;"><small class="s-heading">{{ setting('site.site_name') }} - {{ setting('site.site_slogan') }}</small></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>


    </div>


</body>
</html>