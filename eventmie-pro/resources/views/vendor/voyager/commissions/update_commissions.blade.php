@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                       
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Organiser</th>
                                        <th>Net Bookings</th>
                                        <th>Net Commission</th>
                                        <th>Net Organiser Profit</th>
                                        <th>Month Year</th>
                                        <th>Transferred</th>
                                        <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($commissions))
                                        @foreach($commissions as $key => $data)
                                            <form method="POST"  action="{{route('eventmie.commission_update')}}"                                   class="form-search">
                                                    {{ csrf_field() }}
                                                <tr>
                                                <input type="hidden" class="form-control" name="month_year"  value="{{$data->month_year}}">
                                                <input type="hidden" class="form-control" name="organiser_id" value="{{$data->org_id}}">
                                                    <td>{{$data->organiser_name}}</td>
                                                    <td>{{$data->customer_paid_total}}</td>
                                                    <td>{{$data->admin_commission_total}}</td>
                                                    <td>{{$data->organiser_earning_total}}</td>
                                                    <td>{{ $data->month_year}}</td>
                                                    <td>  
                                                        <div class="form-check">
                                                            <input type="checkbox" name="transferred"  class="form-check-input"  
                                                            {{ $data->transferred > 0 ? 'checked' : '' }}>
                                                        </div>
                                                    </td>
                                                    <td class="no-sort no-click" id="bread-actions">

                                                        <button type="submit" class="btn btn-sm btn-primary     pull-right view">
                                                            <i class="voyager-edit"></i>
                                                             <span class="hidden-xs hidden-sm">
                                                                {{ __('voyager::generic.update') }}
                                                            </span>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </form>

                                        @endforeach
                                    @endif    
                                </tbody>
                            </table>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('css')
@if(config('dashboard.data_tables.responsive'))
    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

