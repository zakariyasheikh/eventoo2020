@extends('eventmie::layouts.app')

@section('title')
    @lang('eventmie-pro::em.booking') @lang('eventmie-pro::em.details')
@endsection

@section('content')
<main>
    <div class="lgx-post-wrapper">
        <section>
            <div class="container">
                <div class="row">
                    
                    {{-- booking details --}}
                    <div class="col-md-6 table-responsive">
                        <h3>@lang('eventmie-pro::em.booking') @lang('eventmie-pro::em.info')</h3>
                        <table class="table table-striped table-hover">
                            <tr>
                                <th>@lang('eventmie-pro::em.order') @lang('eventmie-pro::em.id')</th>
                                <td>{{$booking['order_number']}}</td>
                            </tr>

                            <tr>
                                <th>@lang('eventmie-pro::em.event') @lang('eventmie-pro::em.category')</th>
                                <td>{{$booking['event_category']}}</td>
                            </tr>

                            <tr>
                                <th>@lang('eventmie-pro::em.event')</th>
                                <td>{{$booking['event_title']}}</td>
                            </tr>

                            <tr>
                                <th>@lang('eventmie-pro::em.repetitive')</th>
                                <td>{{$booking['event_repetitive'] == 0 ? 'No' : 'Yes'}}</td>
                            </tr>   

                            <tr>
                                <th>@lang('eventmie-pro::em.ticket') @lang('eventmie-pro::em.price')</th>
                                <td>{{$booking['ticket_price']}}</td>
                            </tr> 
                            <tr>
                                <th>@lang('eventmie-pro::em.total') @lang('eventmie-pro::em.amount') @lang('eventmie-pro::em.paid')</th>
                                <td>{{$booking['net_price'].' '.$currency}}</td>
                            </tr>     
                            
                            <tr>
                                <th>@lang('eventmie-pro::em.start') @lang('eventmie-pro::em.date')</th>
                                <td>{{$booking['event_start_date']}}</td>
                            </tr>   

                            <tr>
                                <th>@lang('eventmie-pro::em.end') @lang('eventmie-pro::em.date')</th>
                                <td>{{$booking['event_end_date']}}</td>
                            </tr>   

                            <tr>
                                <th>@lang('eventmie-pro::em.start') @lang('eventmie-pro::em.time')</th>
                                <td>{{$booking['event_start_time']}}</td>
                            </tr>   

                            <tr>
                                <th>@lang('eventmie-pro::em.end') @lang('eventmie-pro::em.time')</th>
                                <td>{{$booking['event_end_time']}}</td>
                            </tr>   
                            <tr>
                                <th>@lang('eventmie-pro::em.booking') @lang('eventmie-pro::em.date')</th>
                                <td>{{ $booking['created_at'] }}</td>
                            </tr>   

                            <tr>
                                <th>@lang('eventmie-pro::em.booking') @lang('eventmie-pro::em.status')</th>
                                <td ><span class="label label-success">{{$booking['status'] == 0 ? 'Inactive' : 'Active'}}</span></td>
                            </tr>   

                            <tr>
                                <th>@lang('eventmie-pro::em.booking') @lang('eventmie-pro::em.cancellation')</th>
                                @if($booking['booking_cancel'] == 0)
                                    <td><span class="label label-info"> @lang('eventmie-pro::em.no') @lang('eventmie-pro::em.cancellation')</span></td>
                                
                                @elseif($booking['booking_cancel'] == 1)
                                    <td><span class="label label-info">@lang('eventmie-pro::em.cancellation') @lang('eventmie-pro::em.pending')</span></td>    
                            
                                @elseif($booking['booking_cancel'] == 2)
                                    <td><span class="label label-info">@lang('eventmie-pro::em.cancellation') @lang('eventmie-pro::em.approved')</span></td>    

                                @elseif($booking['booking_cancel'] == 3)
                                    <td><span class="label label-info">@lang('eventmie-pro::em.amount') @lang('eventmie-pro::em.redunded')</span></td>
                                @endif     
                            </tr>   

                        </table> 
                    </div>

                    {{-- customer details --}}
                    <div class="col-md-6 table-responsive">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>@lang('eventmie-pro::em.customer') @lang('eventmie-pro::em.info')</h3>
                                <table class="table table-striped table-hover">
                                    <tr>
                                        <th>@lang('eventmie-pro::em.name')</th>
                                        <td>{{$booking['customer_name']}}</td>
                                    </tr>

                                    <tr>
                                        <th>@lang('eventmie-pro::em.email')</th>
                                        <td>{{$booking['customer_email']}}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        
                        {{-- payment information --}}
                        @if(!empty($payment))
                        <div class="row">
                            <div class="col-md-12">
                                <h3>@lang('eventmie-pro::em.payment') @lang('eventmie-pro::em.info')</h3>
                                <table class="table table-striped table-hover">
                                    <tr>
                                        <th>@lang('eventmie-pro::em.transaction') @lang('eventmie-pro::em.id')</th>
                                        <td>{{$payment['txn_id']}}</td>
                                    </tr>

                                    <tr>
                                        <th>@lang('eventmie-pro::em.payment') @lang('eventmie-pro::em.type')</th>
                                        <td>{{$payment['payment_gateway']}}</td>
                                    </tr>

                                    <tr>
                                        <th>@lang('eventmie-pro::em.payment') @lang('eventmie-pro::em.status')</th>
                                        <td>{{$payment['payment_status'] == 'approved' ? 'Success' : 'Unsuccess'}}</td>
                                    </tr>

                                    <tr>
                                        <th>@lang('eventmie-pro::em.total') @lang('eventmie-pro::em.amount') @lang('eventmie-pro::em.paid')</th>
                                        <td>{{$payment['amount_paid']}} {{$payment['currency_code']}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        @endif
                        
                    </div>    
                    
                </div>    
            </div>
        </section>
    </div>
</main>
         
@endsection
