@extends('eventmie::layouts.app')

@section('title')
    @lang('eventmie-pro::em.speaker') - {{ $speaker->fullname }}
@endsection

@section('content')
<main>
    <div class="lgx-post-wrapper">
         <section>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4">
                        <article>
                            <header>
                                <figure>
                                    @if($speaker->image)
                                    <img src="/storage/{{ $speaker->image }}" alt="{{ $speaker->fullname }}" class="img-responsive img-rounded"/>
                                    @else
                                    <img src="{{ eventmie_asset('img/512x512.jpg') }}" alt="{{ $speaker->fullname }}" class="img-responsive img-rounded" />
                                    @endif
                                </figure>
                                <div class="text-area">
                                    <div class="speaker-info">
                                        <h1 class="title">{{ $speaker->fullname }}</h1>
                                        <h4 class="subtitle">{{ $speaker->designation }}</h4>
                                    </div>
                                    <ul class="list-inline lgx-social">
                                        @if($speaker->twitter)
                                        <li><a href="{{ $speaker->twitter }}" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                        @endif
                                        
                                        @if($speaker->facebook)
                                        <li><a href="{{ $speaker->facebook }}" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                        @endif

                                        @if($speaker->linkedin)
                                        <li><a href="{{ $speaker->linkedin }}" target="_blank"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                                        @endif
                                        
                                        @if($speaker->instagram)
                                        <li><a href="{{ $speaker->instagram }}" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                                        @endif

                                        @if($speaker->website)
                                        <li><a href="{{ $speaker->website }}" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </header>
                        </article>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8">
                        <article>
                            <section>{!! $speaker->about !!}</section>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>

@endsection