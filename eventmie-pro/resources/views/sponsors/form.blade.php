@extends('eventmie::layouts.app')

{{-- Page title --}}
@section('title')
    @lang('eventmie-pro::em.manage') @lang('eventmie-pro::em.sponsors')
@endsection

@section('content')

<main>
    <div class="lgx-post-wrapper">
        <section>
            <router-view
                :organiser_id="{{ $organiser_id }}"
                :organisers="{{ json_encode($organisers, JSON_HEX_APOS) }}" 
            ></router-view>
        </section>
    </div>
</main>

@endsection

@section('javascript')
<script>    
    var path = {!! json_encode($path, JSON_HEX_TAG) !!};
</script>
<script type="text/javascript" src="{{ eventmie_asset('js/sponsors_manage.js') }}"></script>
@stop
