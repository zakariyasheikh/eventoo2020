@extends('eventmie::layouts.app')

@section('title')
    @lang('eventmie-pro::em.profile')
@endsection

@section('content')

<main>
    <div class="lgx-post-wrapper">
        <section>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                    
                        <div class="card">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('eventmie.updateAuthUser')}}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group row">
                                        <label class="col-md-3">@lang('eventmie-pro::em.name')</label>
                                        <div class="col-md-9">
                                            <input class="form-control" name="name" type="text" value="{{$user->name}}">
                                            
                                            @if ($errors->has('name'))
                                                <div class="error">{{ $errors->first('name') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3">@lang('eventmie-pro::em.email')</label>
                                        <div class="col-md-9">
                                            <input class="form-control"  name="email" type="email" value="{{$user->email}}">
                                            @if ($errors->has('email'))
                                                <div class="error">{{ $errors->first('email') }}</div>
                                            @endif
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>@lang('eventmie-pro::em.update_password') </h4>
                                    <hr>

                                    <div class="form-group row">
                                        <label class="col-md-3">@lang('eventmie-pro::em.current') @lang('eventmie-pro::em.password')</label>
                                        <div class="col-md-9">
                                            <input class="form-control" name="current" type="password" >
                                            @if ($errors->has('current'))
                                                <div class="error">{{ $errors->first('current') }}</div>
                                            @endif
                                        
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3">@lang('eventmie-pro::em.new') @lang('eventmie-pro::em.password')</label>
                                        <div class="col-md-9">
                                            <input class="form-control" name="password" type="password" >
                                            @if ($errors->has('password'))
                                                <div class="error">{{ $errors->first('password') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3">@lang('eventmie-pro::em.confirm') @lang('eventmie-pro::em.password')</label>
                                        <div class="col-md-9">
                                            <input class="form-control" name="password_confirmation" type="password" >
                                            @if ($errors->has('password_confirmation'))
                                                <div class="error">{{ $errors->first('password_confirmation') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <div class="col-md-9 offset-md-3">
                                            <button class="lgx-btn" type="submit"><i class="fas fa-sd-card"></i> @lang('eventmie-pro::em.save') @lang('eventmie-pro::em.profile')</button>
                                        </div>
                                    </div>
                                </form>

                                <hr>
                                {{-- if logged in user is customer and multi-vendor mode is enabled --}}
                                @if(Auth::user()->hasRole('customer'))
                                @if(setting('multi-vendor.multi_vendor')) 
                                <div class="form-group row">
                                    <label class="col-md-3">@lang('eventmie-pro::em.want_to_create_host')</label>
                                    <div class="col-md-9">
                                        <button type="button" class="lgx-btn lgx-btn-black lgx-btn-sm" data-toggle="modal" data-target="#myModal"><i class="fas fa-person-booth"></i> @lang('eventmie-pro::em.become') @lang('eventmie-pro::em.organiser')</button>
                                    </div>
                                </div>
                                @endif
                                @endif
                                
                                <!-- Modal -->
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">@lang('eventmie-pro::em.become') @lang('eventmie-pro::em.organiser')</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="alert alert-info">
                                                    <h4>@lang('eventmie-pro::em.info')</h4>
                                                    <ul>
                                                        <li>@lang('eventmie-pro::em.organiser_note_1')</li>
                                                        <li>@lang('eventmie-pro::em.organiser_note_2')</li>
                                                        <li>@lang('eventmie-pro::em.organiser_note_3')</li>
                                                        <li>@lang('eventmie-pro::em.organiser_note_4')</li>
                                                    </ul>
                                                </div>
                                                <form class="form-horizontal" action="{{ route('eventmie.updateAuthUserRole')}}" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="role_id" value="3">
                                                    
                                                    <div class="form-group row">
                                                        <label class="col-md-3">@lang('eventmie-pro::em.organization') @lang('eventmie-pro::em.name')</label>
                                                        <div class="col-md-9">
                                                            <input class="form-control" name="organisation" type="text" placeholder="@lang('eventmie-pro::em.brand') @lang('eventmie-pro::em.identity')">
                                                        
                                                            @if ($errors->has('organisation'))
                                                                <div class="error">{{ $errors->first('organisation') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group row">
                                                        <div class="col-md-12 text-right">
                                                            <button type="submit" class="lgx-btn"><i class="fas fa-sd-card"></i> @lang('eventmie-pro::em.submit')</button>
                                                        </div>
                                                    </div>

                                                </form>    
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ eventmie_asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ eventmie_asset('js/bootstrap.min.js') }}"></script>
@stop