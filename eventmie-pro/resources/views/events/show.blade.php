@extends('eventmie::layouts.app')

@section('title', $event->title)
@section('meta_title', $event->meta_title)
@section('meta_keywords', $event->meta_keywords)
@section('meta_description', $event->meta_description)
@section('meta_image', '/storage/'.$event['poster'])
@section('meta_url', url()->current())

    
@section('content')

<!--BANNER-->
<section>
    <div class="lgx-banner event-poster" style="background-image: url({{ '/storage/'.$event['poster'] }});">
        <div class="lgx-banner-style">
            <div class="lgx-inner lgx-inner-fixed">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-banner-info-area">
                                <div class="lgx-banner-info">
                                    <h2 class="title">{{$event->title}}</span></h2>
                                    <h3 class="location"><i class="fas fa-map-marked-alt"></i> {{$event->venue}},{{$event->address}}.</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER -->
        </div>
    </div>
</section>
<!--BANNER END-->

<!--ABOUT-->
<section>
    <div id="lgx-about" class="lgx-about">
        <div class="lgx-inner">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-12 col-sm-12 col-lg-4 offset-lg-1">
                        <div class="lgx-banner-info-area">
                            <div class="lgx-banner-info-circle lgx-info-circle">
                                <div class="info-circle-inner" style="background-image: url({{ eventmie_asset('img/bg-wave-circle.png') }});">
                                    <h3 class="date">
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->start_date)->format('d')}} 
                                        <span>
                                            {{\Carbon\Carbon::createFromFormat('Y-m-d', $event->start_date)->format('M-Y')}}
                                        </span>
                                    </h3>
                                    <div class="lgx-countdown-area">
                                        <!-- Date Format :"Y/m/d" || For Example: 1017/10/5  -->
                                        <div id="lgx-countdown" 
                                            data-date="{{\Carbon\Carbon::createFromFormat('Y-m-d', $event->start_date)
                                                ->format('Y/m/d')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 offset-sm-1 col-sm-10 col-lg-5">
                        <div class="lgx-about-content-area">
                            <div class="lgx-heading">
                                <h2 class="heading">{{ $event['title'] }}</h2>
                                <h3 class="subheading">
                                    <span class="lgx-badge lgx-badge-info">{{ $category['name'] }}</span>
                                    
                                    @if(!empty($free_tickets))
                                        <span class="lgx-badge lgx-badge-success">@lang('eventmie-pro::em.free') @lang('eventmie-pro::em.tickets')</span>
                                    @endif

                                    @if($event->repetitive)
                                        @if($event->repetitive_type == 1)
                                            <span class="lgx-badge lgx-badge-primary">
                                                @lang('eventmie-pro::em.repetitive') @lang('eventmie-pro::em.daily') @lang('eventmie-pro::em.event')
                                            </span>
                                        @elseif($event->repetitive_type == 2)    
                                            <span class="lgx-badge lgx-badge-primary">
                                                @lang('eventmie-pro::em.repetitive') @lang('eventmie-pro::em.weekly') @lang('eventmie-pro::em.event')
                                            </span>
                                        @elseif($event->repetitive_type == 3)    
                                            <span class="lgx-badge lgx-badge-primary">
                                                @lang('eventmie-pro::em.repetitive') @lang('eventmie-pro::em.monthly') @lang('eventmie-pro::em.event')
                                            </span>
                                        @endif    
                                        
                                    @endif
                                    
                                    @if($ended)   
                                        <span class="lgx-badge lgx-badge-danger">@lang('eventmie-pro::em.event') @lang('eventmie-pro::em.ended')</span>
                                    @endif
                                </h3>
                            </div>
                            <div class="lgx-about-content">{!! $event['description'] !!}</div>
                        </div>
                    </div>

                </div>
                <br><br>
                <div class="row">
                    <div class="col-12 col-sm-5 col-md-5 offset-md-1">
                        <div class="lgx-about-service">
                            <div class="lgx-single-service lgx-single-service-color">
                                <span class="icon"><i class="fas fa-map-marked-alt" aria-hidden="true"></i></span>
                                <div class="text-area">
                                    <h2 class="title">@lang('eventmie-pro::em.where')</h2>
                                    <p>
                                        <strong>{{$event->venue}}</strong> <br>
                                        {{$event->address}} {{ $event->zipcode }} <br>
                                        {{ $event->city }}, 
                                        {{ $event->state }}, 
                                        {{ $country['country_name'] }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-5 col-md-5">
                        <div class="lgx-about-service">
                             <div class="lgx-single-service lgx-single-service-color">
                                <span class="icon"><i class="fas fa-stopwatch" aria-hidden="true"></i></span>
                                <div class="text-area">
                                    <h2 class="title">@lang('eventmie-pro::em.when')</h2>
                                    @if(!$event->repetitive)
                                    <p>
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->start_date)->format('d M Y') }}, 
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->start_date)->format('l') }}, 
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:m:s', $event->start_date.''.$event->start_time)->format('h:m A') }}

                                        <br>@lang('eventmie-pro::em.till')<br>

                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->end_date)->format('d M Y') }}, 
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->end_date)->format('l') }}, 
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:m:s', $event->end_date.''.$event->end_time)->format('h:m A') }}
                                    </p>
                                    @else
                                    <p>
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->start_date)->format('d M Y') }}
                                        
                                        <br>@lang('eventmie-pro::em.till')<br>
                                        
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->end_date)->format('d M Y') }}
                                    </p>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
<!--ABOUT END-->

<!--SCHEDULE-->
<section>
    <div id="lgx-schedule" class="lgx-schedule">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-registration-area-simple">
                            <div class="lgx-heading lgx-heading-white">
                                <h2 class="heading">@lang('eventmie-pro::em.get_tickets')</h2>
                                
                                @if($event->repetitive)
                                <h3 class="subheading">@lang('eventmie-pro::em.select_schedule')</h3>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <select-dates 
                        :event="{{ json_encode($event, JSON_HEX_APOS) }}" 
                        :max_ticket_qty="{{ json_encode($max_ticket_qty, JSON_HEX_APOS) }}"
                        :login_user_id="{{ json_encode(\Auth::id(), JSON_HEX_APOS) }}"
                    >
                    </select-dates>
                </div>
                <!--//.ROW-->
            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.INNER -->
    </div>
</section>
<!--SCHEDULE END-->



<!--Event FAQ-->
<section>
    <div id="lgx-about" class="lgx-about">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading">
                            <h2 class="heading">@lang('eventmie-pro::em.event') @lang('eventmie-pro::em.info')</h2>
                        </div>
                    </div>
                    <!--//main COL-->
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-about-content-area text-center">
                            <div class="lgx-about-content">{!! $event['faq'] !!}</div>
                        </div>
                    </div>
                </div>
                <!--//.ROW-->
            </div>
            <!-- //.CONTAINER -->
        </div>
    </div>
</section>
<!--Event FAQ END-->


<!--SPEAKERS-->
<section>
    <div id="lgx-schedule" class="lgx-schedule lgx-schedule-dark">
        <div class="lgx-inner" style="background-image: url({{ eventmie_asset('img/bg-pattern.png') }});">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading lgx-heading-white">
                            <h2 class="heading">@lang('eventmie-pro::em.event') @lang('eventmie-pro::em.speakers')</h2>
                        </div>
                    </div>
                </div>
                <!--//.ROW-->
                <div class="row">
                    
                    @if(!empty($speakers))
                        @foreach($speakers as $key=>$value)
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="lgx-single-speaker">
                                    <figure>
                                        <a class="profile-img" href="{{route('eventmie.events_speaker',[$event->title, str_replace(' ', '-', $value['fullname'])] )}}"
                                        >
                                            @if($value['image'])
                                            <img src="/storage/{{ $value['image'] }}" alt="{{ $value['fullname'] }}"/>
                                            @else
                                            <img src="{{ eventmie_asset('img/512x512.jpg') }}" alt="{{ $value['fullname'] }}"/>
                                            @endif
                                        </a>
                                        
                                        <figcaption>
                                            <div class="social-group">
                                                @if($value['twitter'])
                                                <a class="sp-tw" href="{{ $value['twitter'] }}" target="_blank"><i class="fab fa-twitter"></i></a>
                                                @endif

                                                @if($value['facebook'])
                                                <a class="sp-fb" href="{{ $value['facebook'] }}" target="_blank"><i class="fab fa-facebook"></i></a>
                                                @endif

                                                @if($value['instagram'])
                                                <a class="sp-insta" href="{{ $value['instagram'] }}" target="_blank"><i class="fab fa-instagram"></i></a>
                                                @endif

                                                @if($value['linkedin'])
                                                <a class="sp-in" href="{{ $value['linkedin'] }}" target="_blank"><i class="fab fa-linkedin"></i></a>
                                                @endif

                                                @if($value['website'])
                                                <a class="sp-in" href="{{ $value['website'] }}" target="_blank"><i class="fas fa-globe"></i></a>
                                                @endif
                                            </div>
                                            <div class="speaker-info">
                                            <h3 class="title">
                                                <a href="{{route('eventmie.events_speaker',[$event->slug, str_replace(' ', '-', $value['fullname'])] )}}">
                                                    {{$value['fullname']}}
                                                </a>
                                            </h3>
                                            <h4 class="subtitle">{{$value['designation']}}</h4>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        
                        @endforeach
                    @endif
                </div>
            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.INNER -->
    </div>
</section>
<!--SPEAKERS END-->

<!--SPONSORED-->
@if(!empty($sponsors))
<section>
    <div class="lgx-news">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading">
                            <h2 class="heading">@lang('eventmie-pro::em.event') @lang('eventmie-pro::em.sponsors')</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="sponsors-area">
                            @foreach($sponsors as $key=>$value)
                            <div class="single"><img src="/storage/{{ $value['image'] }}" alt="sponsor"/></div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!--SPONSORED END-->

<!--PHOTO GALLERY-->
@if(!empty($event->images))
<section>
    <div id="lgx-photo-gallery" class="lgx-gallery-popup lgx-photo-gallery-normal lgx-photo-gallery-black">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading lgx-heading-white">
                            <h2 class="heading">@lang('eventmie-pro::em.event') @lang('eventmie-pro::em.gallery')</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-gallery-area">
                            @foreach(json_decode($event->images, true) as $item)
                            <div  class="lgx-gallery-single">
                                <figure>
                                    <img title="{{ $event->title }}" src="/storage/{{ $item }}" alt="{{ $event->slug }}" style="height: 280px;"/>
                                </figure>
                            </div> <!--Single photo-->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!--PHOTO GALLERY END-->



<!--Event Video-->
@if(!empty($event->video_link))
<section>
    <div id="lgx-travelinfo" class="lgx-travelinfo">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading">
                            <h2 class="heading">@lang('eventmie-pro::em.watch') @lang('eventmie-pro::em.trailer')</h2>
                        </div>
                    </div>
                    <!--//main COL-->
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-10">
                        <iframe src="https://www.youtube.com/embed/{{ $event->video_link }}" allowfullscreen style="width: 100%; height: 500px; border-radius: 16px; border: none;"></iframe>
                    </div>
                </div>
                <!--//.ROW-->
            </div>
            <!-- //.CONTAINER -->
        </div>
    </div>
</section>
@endif
<!--Event Video END-->


<!--GOOGLE MAP-->
<div class="innerpage-section g-map-wrapper">
    <div class="lgxmapcanvas map-canvas-default"> 
        
        <g-component :lat="{{ json_encode($event->latitude, JSON_HEX_APOS) }}" :lng="{{ json_encode($event->longitude, JSON_HEX_APOS) }}" >
        </g-component>

    </div>
</div>
<!--GOOGLE MAP END-->

@endsection

@section('javascript')


<script type="text/javascript" src="{{ eventmie_asset('js/events_show.js') }}"></script>

<script type="text/javascript">
    var google_map_key = {!! json_encode( $google_map_key) !!};

</script>

@stop
