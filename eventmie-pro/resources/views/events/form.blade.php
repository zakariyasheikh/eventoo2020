@extends('eventmie::layouts.app')

{{-- Page title --}}
@section('title')
    @if(empty($event)) 
        @lang('eventmie-pro::em.create') @lang('eventmie-pro::em.event')
    @else
        @lang('eventmie-pro::em.update') @lang('eventmie-pro::em.event')
    @endif
@endsection

    
@section('content')

<main>
    <!--SCHEDULE-->
    <div class="lgx-post-wrapper">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-tab">
                            <tabs-component :event_id="{{ !empty($event) ? $event->id : 0 }}"  :organiser_id="{{$organiser_id}}"></tabs-component>
                            
                            <div class="tab-content lgx-tab-content lgx-tab-content-event">
                                <router-view 
                                    :is_admin="{{ json_encode(Auth::user()->hasRole('admin'))}}"
                                    :organisers="{{ json_encode($organisers, JSON_HEX_APOS) }}" 
                                    :organiser_id="{{$organiser_id}}"
                                >
                                    
                                </router-view>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--SCHEDULE END-->
</main>

@endsection

@section('javascript')
<script type="text/javascript" src="{{ eventmie_asset('js/events_manage.js') }}"></script>

@stop
