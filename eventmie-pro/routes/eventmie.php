<?php

use Classiebit\Eventmie\Facades\Eventmie;

/*
|--------------------------------------------------------------------------
| Package Routes
|--------------------------------------------------------------------------
|
*/

$namespace = !empty(config('eventmie.controllers.namespace')) ? '\\'.config('eventmie.controllers.namespace') : '\Classiebit\Eventmie\Http\Controllers';

Route::group([
    // 'namespace' => $namespace,
    'prefix' => config('eventmie.route.prefix'),
    'as'    => 'eventmie.',
    
    
], function() use($namespace) {
        
    // Localization --------------------------------------------------------------------------
    Route::get('/assets/js/eventmie_lang', function () {
        // default lang
        $lang = config('app.locale');
           
        // user lang
        if(session('my_lang'))
        {
            $lang = session('my_lang');    
            \App::setLocale(session('my_lang'));
        }
        
        $strings['em'] = \Lang::get('eventmie-pro::em');
         
        header('Content-Type: text/javascript; charset=UTF-8');
        echo('window.i18n = ' . json_encode($strings) . ';');
        
        exit();
    })->name('eventmie_lang');
    // --------------------------------------------------------------------------

    // eventmie lang selector Route---------------------------------------------------------------------------
    Route::get('/lang/{lang?}', $namespace.'\EventmieController@change_lang')->name('change_lang');

    // Package Asset Routes ------------------------------------------------------------
    Route::get('eventmie-assets', $namespace.'\EventmieController@assets')->name('eventmie_assets');
    // --------------------------------------------------------------------------

    // Auth route --------------------------------------------------------------------
    Auth::routes();

    // Login --------------------------------------------------------------------------
    Route::get('login', $namespace.'\Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', $namespace.'\Auth\LoginController@login')->name('login_post');
    // --------------------------------------------------------------------------

    // Logout route --------------------------------------------------------------------
    Route::match(['get', 'post'], '/logout', $namespace.'\EventmieController@logout')->name('logout');
    // --------------------------------------------------------------------------
    
    // Registration Routes------------------------------------------------------------------------------------
    Route::get('register', $namespace.'\Auth\RegisterController@showRegistrationForm')->name('register_show');
    Route::post('register', $namespace.'\Auth\RegisterController@register')->name('register');
    
    //--------------------------------------------------------------------------------------------------


    // Forgot password Password-------------------------------------------------------------------
    Route::get('password/reset',  $namespace.'\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', $namespace.'\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('forgot/password/reset/{token}', $namespace.'\Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('forgot/password/reset/post',   $namespace.'\Auth\ResetPasswordController@reset')->name('password.reset_post');
    //-----------------------------------------------------------------------------
    
    
    
    // --------------------------------------------------------------------------
    // Package Controllers Routes ******************************************************
    // --------------------------------------------------------------------------

    // Welcome Routes -------------------------------------------------------------
    Route::get('/', $namespace."\WelcomeController@index")->name('welcome');
    Route::get('/home', function() {
        return redirect()->route('eventmie.welcome');
    });
    // --------------------------------------------------------------------------
    
    // Static Pages Routes -------------------------------------------------------------
    Route::get('/page/{page}', $namespace."\PagesController@view")->name('page'); 
    // --------------------------------------------------------------------------

    // Sponsors Routes -----------------------------------------------------------------
    Route::prefix('/mysponsors')->group(function () use ($namespace)  {
        $controller = $namespace.'\SponsorsController';

        Route::post('/api', "$controller@sponsors")->name('sponsors');         // axios route
        Route::get('/', "$controller@form")->name('sponsors_form');
        Route::post('/api/add', "$controller@store")->name('sponsors_store');       // axios route
        Route::post('/api/delete', "$controller@delete")->name('sponsors_delete');  // axios route
        Route::post('/api/selected/sponsors', "$controller@selected_event_sponsors")->name('selected_sponsors');  // axios route
    });
    // --------------------------------------------------------------------------

    // Speakers Routes -----------------------------------------------------------------
    Route::prefix('/myspeakers')->group(function () use ($namespace)  {
        $controller = $namespace.'\SpeakersController';

        Route::get('/', "$controller@form")->name('speakers_form');         
        Route::post('/api', "$controller@speakers")->name('speakers');          // axios route
        Route::post('/api/add', "$controller@store")->name('speakers_store');       // axios route
        Route::post('/api/delete', "$controller@delete")->name('speakers_delete');  // axios route
        Route::post('/api/selected/speakers', "$controller@selected_event_speakers")->name('selected_speakers');  // axios route
    });
    // --------------------------------------------------------------------------

    // Tickets Routes -----------------------------------------------------------------
    Route::prefix('/tickets')->group(function () use ($namespace)  {
        $controller = $namespace.'\TicketsController';

        Route::post('/api', "$controller@tickets")->name('tickets');                // axios route
        Route::get('/api/taxes', "$controller@taxes")->name('tickets_taxes');       // axios route
        Route::post('/api/store', "$controller@store")->name('tickets_store');          // axios route
        Route::post('/api/delete', "$controller@delete")->name('tickets_delete');       // axios route
    });
    // --------------------------------------------------------------------------

    // Tickets Routes -----------------------------------------------------------------
    Route::prefix('/schedules')->group(function () use ($namespace)  {
        $controller = $namespace.'\SchedulesController';

        // while creating an event
        Route::post('/api', "$controller@schedules")->name('schedules');                        // axios route 
        Route::post('/api/event_schedule', "$controller@event_schedule")->name('event_schedule');   // axios route
        
    });
    // --------------------------------------------------------------------------

    // Events Routes -----------------------------------------------------------------
    Route::prefix('/events')->group(function () use ($namespace) {
        $controller = $namespace.'\EventsController';
        
        Route::get('/', "$controller@index")->name('events_index');
        
        Route::get('/api/get_events', "$controller@events")->name('events'); // axios route        
        // Wildcard routes
        Route::get('/{event}', "$controller@show")->name('events_show');
        
        // show speaker
        Route::get('/{event}/speaker/{speaker_name}', "$controller@speaker")->name('events_speaker');

        Route::get('/api/categories', "$controller@categories")->name('myevents_categories');   // axios route  

        Route::post('/api/check/session', "$controller@check_session")->name('check_session');   // axios route  
        
    });
    // --------------------------------------------------------------------------
    
    // Bookings Routes -----------------------------------------------------------------
    Route::prefix('/bookings')->group(function () use ($namespace)  {

        $controller = $namespace.'\BookingsController';

        Route::post('/api/get_tickets', "$controller@get_tickets")->name('bookings_get_tickets');  // axios route
        Route::post('/api/book_tickets', "$controller@book_tickets")->name('bookings_book_tickets');   // axios route
        
        // Paypal Checkout
        Route::match(['get', 'post'], '/paypal/callback', "$controller@paypal_callback")->name('bookings_paypal_callback');  
    });
    // --------------------------------------------------------------------------
    
    // My Bookings Routes For Customer  -----------------------------------------------------------------
    Route::prefix('/mybookings')->group(function () use($namespace) {
        
        $controller = $namespace.'\MyBookingsController';

        Route::get('/', "$controller@index")->name('mybookings_index');                     
        Route::get('/api/get_mybookings', "$controller@mybookings")->name('mybookings');    // axios route
        Route::post('/api/cancel', "$controller@cancel")->name('mybookings_cancel');            // axios route

    });
    // --------------------------------------------------------------------------

    
    // My Bookings Routes For Organiser-------------------------------------------------
    Route::prefix('/bookings')->group(function () use ($namespace)  {
        
        $controller = $namespace.'\OBookingsController';
        
        Route::get('/', "$controller@index")->name('obookings_index');  
        Route::get('/api/organiser_bookings', "$controller@organiser_bookings")->name('obookings_organiser_bookings');               // axios route
        Route::post('/api/organiser_bookings_edit', "$controller@organiser_bookings_edit")->name('obookings_organiser_bookings_edit');   // axios route 
        Route::get('/booking/{id}', "$controller@organiser_bookings_show")->name('obookings_organiser_bookings_show');     
        Route::get('/delete/{id}', "$controller@delete_booking")->name('obookings_organiser_booking_delete');     
    });
    // --------------------------------------------------------------------------

    
    // Events Routes -----------------------------------------------------------------
    Route::prefix('/myevents')->group(function () use ($namespace) {
        $controller = $namespace.'\MyEventsController';
        
        Route::get('/', "$controller@index")->name('myevents_index');
        Route::get('/api/get_myevents', "$controller@get_myevents")->name('myevents');                  // axios route
        
        // event create and edit
        Route::get('/manage/{slug?}', "$controller@form")->name('myevents_form');  
        
        Route::post('/api/store', "$controller@store")->name('myevents_store');                             // axios route
        Route::post('/api/store_media', "$controller@store_media")->name('myevents_store_media');           // axios route
        Route::post('/api/store_location', "$controller@store_location")->name('myevents_store_location');  // axios route
        Route::post('/api/store_timing', "$controller@store_timing")->name('myevents_store_timing');        // axios route
        Route::post('/api/store_sponsors_speakers', "$controller@store_sponsors_speakers")->name('myevents_store_sponsors_speakers');  // axios route
        Route::post('/api/store_seo', "$controller@store_seo")->name('myevents_store_seo');  // axios route
        
        
        Route::get('/api/countries', "$controller@countries")->name('myevents_countries');      // axios route  

        Route::post('/api/get_myevent', "$controller@get_user_event")->name('get_myevent');      // axios route  

        Route::post('/api/publish_myevent', "$controller@event_publish")->name('publish_myevent');      // axios route  

        // event disable
        Route::post('/api/disable', "$controller@disable_event")->name('myevents_disable');       // axios route 

        // event delete
        Route::get('/delete/{slug}', "$controller@delete_event")->name('delete_event');       // axios route 

        Route::get('/export_attendees/{slug}', "$controller@export_attendees")->name('export_attendees');      // axios route  
     
    });
    //---------------------------------------------------------------------------------------------------------------

    // Notification Route ----------------------------------------------------------------------
    Route::prefix('/notifications')->group(function () use ($namespace)  {
        
        // make read notification 
        Route::get('/read/{n_type}', function($n_type){

            if($n_type) {
                        
                $id   = \Auth::id();
                $user = \Classiebit\Eventmie\Models\User::find($id);
                $user->unreadNotifications->where('n_type', $n_type)->markAsRead();
                
            }
            // admin redirect to dashboard
            if(\Auth::user()->hasRole('admin'))
            {
                if($n_type == "user")
                    return redirect()->route('voyager.users.index');
                else if($n_type == "bookings" || $n_type == "cancel")
                    return redirect()->route('voyager.bookings.index');
                else if($n_type == "events")
                    return redirect()->route('voyager.events.index');
                else
                    return redirect()->route('voyager.dashboard');
            }

            // organiser redirect to notification's related page
            if(\Auth::user()->hasRole('organiser'))
            {
                // create events notification
                if($n_type == "events")
                    return redirect()->route('eventmie.myevents_index');
                
                // create booking notification
                if($n_type == "bookings" || $n_type == "cancel" )
                    return redirect()->route('eventmie.obookings_index');    
            }

            // customer redirect to notification's related page
            if(\Auth::user()->hasRole('customer'))
            {
                // create events notification
                if($n_type == "user")
                    return redirect()->route('eventmie.profile');
                
                // create booking notification
                if($n_type == "bookings" || $n_type == "cancel" )
                    return redirect()->route('eventmie.mybookings_index');    
            }
            
            // customer and organiser redirect to welcome page
            return redirect()->route('eventmie.welcome');
            
        })->name('notify_read');
        
        
    });
    //------------------------------------------------------------------------------------------------------------------

    //Profile Route ----------------------------------------------------------------------------
    Route::prefix('/profile')->group(function () use ($namespace) {
            
        $controller = $namespace.'\ProfileController';
        // view    
        Route::get('/', "$controller@index")->name('profile');
        
        Route::post('/updateAuthUser',"$controller@updateAuthUser")->name('updateAuthUser');
        
        Route::post('/updateAuthUserRole',"$controller@updateAuthUserRole")->name('updateAuthUserRole');    
    
    });
    //------------------------------------------------------------------------------------------

    
    //Profile Route ----------------------------------------------------------------------------
    Route::prefix('/blogs')->group(function () use ($namespace)  {
        
        $controller = $namespace.'\BlogsController';
        
        // listing post 
        Route::get('/', "$controller@get_posts")->name('get_posts');
        
        // view post
        Route::get('/{slug}',"$controller@view")->name('post_view');
    
    });
    //-----------------------------------------------------------------------------------------
    
    
    
    //Contact Route ----------------------------------------------------------------------------
    Route::prefix('/contact')->group(function () use ($namespace) {
            
        $controller = $namespace.'\ContactController';
        
        // contact view page load 
        Route::get('/', "$controller@index")->name('contact');

        // contact save into contacts tables 
        Route::post('/save', "$controller@store_contact")->name('store_contact');
            
    });
    //-------------------------------------------------------------------------------------------

    // Oauth login ---------------------------------------------------------------------------------------
    Route::get('/login/{social}', $namespace.'\Auth\LoginController@socialLogin')
    ->where('social', 'facebook|google')->name('oauth_login');

    Route::get('/login/{social}/callback', $namespace.'\Auth\LoginController@handleProviderCallback')
    ->where('social', 'facebook|google')->name('oauth_callback');
    //-------------------------------------------------------------------------------------------------------
        
    // commission update
    Route::post('/commission/update', $namespace.'\Voyager\CommissionsController@commission_update')->name('commission_update');

    // Download Ticket (public)-------------------------------------------------
    Route::prefix('/download')->group(function () use ($namespace)  {
        
        $controller = $namespace.'\DownloadsController';
        
        Route::get('/ticket/{id}/{order_number}', "$controller@index")->name('downloads_index');  
        
    });
    // --------------------------------------------------------------------------
    
});
// --------------------------------------------------------------------------

// Voyager Routes -----------------------------------------------------------------
Route::group([
    'namespace' => $namespace.'\Voyager',
    'prefix' => config('eventmie.route.prefix').'/'.config('eventmie.route.admin_prefix'),
], function () {
    \Voyager::routes();

});

// --------------------------------------------------------------------------

 