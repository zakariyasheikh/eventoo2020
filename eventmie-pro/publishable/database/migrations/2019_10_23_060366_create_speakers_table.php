<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSpeakersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('speakers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('fullname', 256);
			$table->string('designation', 256);
			$table->text('about', 65535);
			$table->string('phone', 24)->nullable();
			$table->string('email', 128)->nullable();
			$table->string('facebook', 512)->nullable();
			$table->string('instagram', 512)->nullable();
			$table->string('twitter', 512)->nullable();
			$table->string('linkedin', 512)->nullable();
			$table->string('website', 512)->nullable();
			$table->string('image', 512)->nullable();
			$table->timestamps();
			$table->boolean('status')->nullable()->default(0);
			$table->integer('user_id')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('speakers');
	}

}
