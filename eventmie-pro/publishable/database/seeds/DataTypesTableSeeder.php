<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class DataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $dataType = $this->dataType('id', 1);
        if (!$dataType->exists) 
        {
            \DB::table('data_types')->insert(array (
                0 => 
                array (
                    'id' => 1,
                    'name' => 'users',
                    'slug' => 'users',
                    'display_name_singular' => 'User',
                    'display_name_plural' => 'Users',
                    'icon' => 'voyager-person',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\User',
                    'policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy',
                    'controller' => '\\Classiebit\\Eventmie\\Http\\Controllers\\Voyager\\VoyagerUserController',
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 1,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":"email","scope":null}',
                    'created_at' => '2018-12-21 10:25:07',
                    'updated_at' => '2019-11-14 10:51:48',
                ),
                1 => 
                array (
                    'id' => 2,
                    'name' => 'menus',
                    'slug' => 'menus',
                    'display_name_singular' => 'Menu',
                    'display_name_plural' => 'Menus',
                    'icon' => 'voyager-list',
                    'model_name' => 'TCG\\Voyager\\Models\\Menu',
                    'policy_name' => NULL,
                    'controller' => NULL,
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 0,
                    'details' => '{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}',
                    'created_at' => '2018-12-21 10:25:07',
                    'updated_at' => '2019-11-14 10:50:32',
                ),
                2 => 
                array (
                    'id' => 3,
                    'name' => 'roles',
                    'slug' => 'roles',
                    'display_name_singular' => 'Role',
                    'display_name_plural' => 'Roles',
                    'icon' => 'voyager-lock',
                    'model_name' => 'TCG\\Voyager\\Models\\Role',
                    'policy_name' => NULL,
                    'controller' => NULL,
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 0,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":"name","scope":null}',
                    'created_at' => '2018-12-21 10:25:07',
                    'updated_at' => '2019-11-14 10:51:11',
                ),
                3 => 
                array (
                    'id' => 5,
                    'name' => 'posts',
                    'slug' => 'posts',
                    'display_name_singular' => 'Post',
                    'display_name_plural' => 'Posts',
                    'icon' => 'voyager-news',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Post',
                    'policy_name' => 'TCG\\Voyager\\Policies\\PostPolicy',
                    'controller' => NULL,
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 1,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}',
                    'created_at' => '2018-12-21 10:25:08',
                    'updated_at' => '2019-11-14 10:51:01',
                ),
                4 => 
                array (
                    'id' => 6,
                    'name' => 'pages',
                    'slug' => 'pages',
                    'display_name_singular' => 'Page',
                    'display_name_plural' => 'Pages',
                    'icon' => 'voyager-file-text',
                    'model_name' => 'TCG\\Voyager\\Models\\Page',
                    'policy_name' => NULL,
                    'controller' => NULL,
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 0,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}',
                    'created_at' => '2018-12-21 10:25:08',
                    'updated_at' => '2019-11-14 10:50:46',
                ),
                5 => 
                array (
                    'id' => 7,
                    'name' => 'events',
                    'slug' => 'events',
                    'display_name_singular' => 'Event',
                    'display_name_plural' => 'Events',
                    'icon' => 'voyager-calendar',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Event',
                    'policy_name' => NULL,
                    'controller' => '\\Classiebit\\Eventmie\\Http\\Controllers\\Voyager\\EventsController',
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 1,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":"title","scope":null}',
                    'created_at' => '2018-12-22 08:54:46',
                    'updated_at' => '2019-11-14 10:53:14',
                ),
                6 => 
                array (
                    'id' => 15,
                    'name' => 'categories',
                    'slug' => 'categories',
                    'display_name_singular' => 'Category',
                    'display_name_plural' => 'Categories',
                    'icon' => 'voyager-categories',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Category',
                    'policy_name' => NULL,
                    'controller' => NULL,
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 1,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"asc","default_search_key":"name","scope":null}',
                    'created_at' => '2018-12-24 09:09:20',
                    'updated_at' => '2019-11-14 10:49:09',
                ),
                7 => 
                array (
                    'id' => 16,
                    'name' => 'speakers',
                    'slug' => 'speakers',
                    'display_name_singular' => 'Speaker',
                    'display_name_plural' => 'Speakers',
                    'icon' => 'voyager-megaphone',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Speaker',
                    'policy_name' => NULL,
                    'controller' => NULL,
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 1,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":"fullname","scope":null}',
                    'created_at' => '2019-01-11 06:22:59',
                    'updated_at' => '2019-11-14 10:51:20',
                ),
                8 => 
                array (
                    'id' => 17,
                    'name' => 'sponsors',
                    'slug' => 'sponsors',
                    'display_name_singular' => 'Sponsor',
                    'display_name_plural' => 'Sponsors',
                    'icon' => 'voyager-medal-rank-star',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Sponsor',
                    'policy_name' => NULL,
                    'controller' => NULL,
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 1,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":"name","scope":null}',
                    'created_at' => '2019-01-11 10:27:39',
                    'updated_at' => '2019-11-14 10:51:26',
                ),
                9 => 
                array (
                    'id' => 20,
                    'name' => 'taxes',
                    'slug' => 'taxes',
                    'display_name_singular' => 'Tax',
                    'display_name_plural' => 'Taxes',
                    'icon' => 'voyager-documentation',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Tax',
                    'policy_name' => NULL,
                    'controller' => NULL,
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 1,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"asc","default_search_key":"title","scope":null}',
                    'created_at' => '2019-06-01 05:00:21',
                    'updated_at' => '2019-11-14 10:51:32',
                ),
                10 => 
                array (
                    'id' => 23,
                    'name' => 'banners',
                    'slug' => 'banners',
                    'display_name_singular' => 'Banner',
                    'display_name_plural' => 'Banners',
                    'icon' => 'voyager-photo',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Banner',
                    'policy_name' => NULL,
                    'controller' => NULL,
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 0,
                    'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                    'created_at' => '2019-07-08 11:11:39',
                    'updated_at' => '2019-11-14 10:48:05',
                ),
                11 => 
                array (
                    'id' => 25,
                    'name' => 'contacts',
                    'slug' => 'contacts',
                    'display_name_singular' => 'Contact',
                    'display_name_plural' => 'Contacts',
                    'icon' => 'voyager-mail',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Contact',
                    'policy_name' => NULL,
                    'controller' => '\\Classiebit\\Eventmie\\Http\\Controllers\\Voyager\\ContactsController',
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 1,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":"name","scope":null}',
                    'created_at' => '2019-07-09 08:52:22',
                    'updated_at' => '2019-11-14 10:50:16',
                ),
                12 => 
                array (
                    'id' => 27,
                    'name' => 'bookings',
                    'slug' => 'bookings',
                    'display_name_singular' => 'Booking',
                    'display_name_plural' => 'Bookings',
                    'icon' => 'voyager-dollar',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Booking',
                    'policy_name' => NULL,
                    'controller' => '\\Classiebit\\Eventmie\\Http\\Controllers\\Voyager\\BookingsController',
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 1,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":"event_title","scope":null}',
                    'created_at' => '2019-08-17 05:29:55',
                    'updated_at' => '2019-11-14 10:49:00',
                ),
                13 => 
                array (
                    'id' => 29,
                    'name' => 'commissions',
                    'slug' => 'commissions',
                    'display_name_singular' => 'Commission',
                    'display_name_plural' => 'Commissions',
                    'icon' => 'voyager-wallet',
                    'model_name' => 'Classiebit\\Eventmie\\Models\\Commission',
                    'policy_name' => NULL,
                    'controller' => '\\Classiebit\\Eventmie\\Http\\Controllers\\Voyager\\CommissionsController',
                    'description' => NULL,
                    'generate_permissions' => 1,
                    'server_side' => 0,
                    'details' => '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":null,"scope":null}',
                    'created_at' => '2019-08-18 08:18:13',
                    'updated_at' => '2019-11-14 10:49:54',
                ),
            ));
        }    
        
        
    }

    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}