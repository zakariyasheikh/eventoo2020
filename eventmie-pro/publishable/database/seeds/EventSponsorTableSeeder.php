<?php

use Illuminate\Database\Seeder;

class EventSponsorTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('event_sponsor')->delete();
        
        \DB::table('event_sponsor')->insert(array (
            0 => 
            array (
                'event_id' => 1,
                'sponsor_id' => 1,
            ),
            1 => 
            array (
                'event_id' => 1,
                'sponsor_id' => 2,
            ),
            2 => 
            array (
                'event_id' => 1,
                'sponsor_id' => 3,
            ),
        ));
        
        
    }
}