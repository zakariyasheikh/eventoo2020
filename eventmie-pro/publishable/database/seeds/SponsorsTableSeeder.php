<?php

use Illuminate\Database\Seeder;
use Classiebit\Eventmie\Models\Sponsor;

class SponsorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $sponsor = $this->sponsor('id', 1);
        if (!$sponsor->exists) 
        {
            \DB::table('sponsors')->insert(array (
                0 => 
                array (
                    'id' => 1,
                    'image' => 'sponsors/September2019/fs2oFKcEt9r78aviykAh.png',
                    'name' => 'Music Studio Records',
                    'website' => 'https://abcxyz.com',
                    'created_at' => '2019-09-02 11:34:53',
                    'updated_at' => '2019-09-02 11:34:53',
                    'status' => 1,
                    'user_id' => 2,
                ),
                1 => 
                array (
                    'id' => 2,
                    'image' => 'sponsors/September2019/xvzS4hDxM77wdsfvtXtj.png',
                    'name' => 'Brandna Taglin',
                    'website' => 'https://abcxyz.com',
                    'created_at' => '2019-09-02 11:35:14',
                    'updated_at' => '2019-09-02 11:35:14',
                    'status' => 1,
                    'user_id' => 2,
                ),
                2 => 
                array (
                    'id' => 3,
                    'image' => 'sponsors/September2019/QN3mh3H3hpkb4tpLDXc7.png',
                    'name' => 'Norwa',
                    'website' => 'https://abcxyz.com',
                    'created_at' => '2019-09-02 11:35:29',
                    'updated_at' => '2019-09-02 11:35:29',
                    'status' => 1,
                    'user_id' => 2,
                ),
            ));
        }
        
    }

    protected function sponsor($field, $for)
    {
        return Sponsor::firstOrNew([$field => $for]);
    }
}