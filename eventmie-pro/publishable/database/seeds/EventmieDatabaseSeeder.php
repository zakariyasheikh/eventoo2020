<?php

use Illuminate\Database\Seeder;
use Classiebit\Eventmie\Traits\Seedable;

class EventmieDatabaseSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('PermissionsTableSeeder');
        $this->seed('RolesTableSeeder');
        $this->seed('TranslationsTableSeeder');
        $this->seed('TaxesTableSeeder');
        $this->seed('SettingsTableSeeder');
        $this->seed('PostsTableSeeder');
        $this->seed('PagesTableSeeder');
        $this->seed('CurrenciesTableSeeder');
        $this->seed('BannersTableSeeder');
        $this->seed('CategoriesTableSeeder');
        $this->seed('CountriesTableSeeder');
        $this->seed('DataTypesTableSeeder');
        $this->seed('DataRowsTableSeeder');
        $this->seed('EventsTableSeeder');
        $this->seed('SpeakersTableSeeder');
        $this->seed('SponsorsTableSeeder');
        $this->seed('EventSpeakerTableSeeder');
        $this->seed('EventSponsorTableSeeder');
        $this->seed('MenusTableSeeder');
        $this->seed('MenuItemsTableSeeder');
        $this->seed('PermissionRoleTableSeeder');
        $this->seed('UsersTableSeeder');
        $this->seed('TicketsTableSeeder');
    }
}
