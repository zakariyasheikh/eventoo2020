<?php

use Illuminate\Database\Seeder;

class EventSpeakerTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('event_speaker')->delete();
        
        \DB::table('event_speaker')->insert(array (
            0 => 
            array (
                'event_id' => 1,
                'speaker_id' => 1,
            ),
            1 => 
            array (
                'event_id' => 1,
                'speaker_id' => 2,
            ),
            2 => 
            array (
                'event_id' => 1,
                'speaker_id' => 3,
            ),
        ));
        
        
    }
}