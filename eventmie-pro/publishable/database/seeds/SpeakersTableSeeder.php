<?php

use Illuminate\Database\Seeder;
use Classiebit\Eventmie\Models\Speaker;

class SpeakersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $speaker = $this->speaker('id', 1);
        if (!$speaker->exists) 
        {
        \DB::table('speakers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'fullname' => 'Jordyn Bryan',
                'designation' => 'Founder & CEO',
                'about' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fusce id velit ut tortor pretium viverra suspendisse potenti nullam. Auctor urna nunc id cursus metus aliquam eleifend. Elit ut aliquam purus sit amet luctus.</p>',
                'phone' => '(906)-689-2250',
                'email' => 'jordynbryan@company.com',
                'facebook' => 'https://facebook.com',
                'instagram' => 'https://instagram.com',
                'twitter' => 'https://twitter.com',
                'linkedin' => 'https://linkedin.com',
                'website' => 'www.abcxyz.com',
                'image' => 'speakers/September2019/dGjSmjqMOM1zl0p2t0ca.jpg',
                'created_at' => '2019-09-02 09:41:55',
                'updated_at' => '2019-09-16 08:46:53',
                'status' => 1,
                'user_id' => 2,
            ),
            1 => 
            array (
                'id' => 2,
                'fullname' => 'Deacon Duffy',
                'designation' => 'Engineer & Businessman',
                'about' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fusce id velit ut tortor pretium viverra suspendisse potenti nullam. Auctor urna nunc id cursus metus aliquam eleifend. Elit ut aliquam purus sit amet luctus.</p>',
                'phone' => '(906)-689-2250',
                'email' => 'deaconduffy@mail.com',
                'facebook' => 'https://facebook.com',
                'instagram' => 'https://instagram.com',
                'twitter' => 'https://twitter.com',
                'linkedin' => 'https://linkedin.com',
                'website' => 'http://www.abcxyz.com',
                'image' => 'speakers/September2019/NOY9stBMP2wEYElcvHhO.jpg',
                'created_at' => '2019-09-02 09:43:30',
                'updated_at' => '2019-09-16 08:46:40',
                'status' => 1,
                'user_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'fullname' => 'Moshe Mayer',
                'designation' => 'Sports Man',
                'about' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fusce id velit ut tortor pretium viverra suspendisse potenti nullam. Auctor urna nunc id cursus metus aliquam eleifend. Elit ut aliquam purus sit amet luctus.</p>',
                'phone' => '(906)-689-2250',
                'email' => 'moshemayer@mail.com',
                'facebook' => 'https://facebook.com',
                'instagram' => 'https://instagram.com',
                'twitter' => 'https://twitter.com',
                'linkedin' => 'https://linkedin.com',
                'website' => 'https://abcxyz.com',
                'image' => 'speakers/September2019/EoMaxzXob2VuFHUejcGq.jpg',
                'created_at' => '2019-09-02 09:44:38',
                'updated_at' => '2019-09-16 08:46:13',
                'status' => 1,
                'user_id' => 2,
            ),
        ));
        
        }
    }

    protected function speaker($field, $for)
    {
        return Speaker::firstOrNew([$field => $for]);
    }
}